package calculator.test;

import static org.junit.Assert.*;

import java.math.BigInteger;

import org.junit.Test;
import org.junit.runner.RunWith;

import calculator.util.Permutations;
import junitparams.*;
import static junitparams.JUnitParamsRunner.$;

@RunWith(JUnitParamsRunner.class)
public class PermutationsTest {
	private Permutations  perm = new Permutations();

	@Parameters
	private static final Object[] ValidPermutation() {
		return $(
				$(10, 5, true, true, BigInteger.valueOf(100000)),
				$(10, 5, true, false, BigInteger.valueOf(2002)),
				$( 10, 5, false, true, BigInteger.valueOf(30240)),
				$(10, 5, false, false, BigInteger.valueOf(252))
				);
	}

	@Parameters
	private static final Object[] InValidPermutation() {
		return $(
				$(5, 10, false, true),
				$(5, 10, false, false),
				$(-5, -10, true, true),
				$(-5, 10, true, true),
				$(5, -10, true, true)
				);
	}
	
	@Test
	@Parameters(method="ValidPermutation")
	public void testValidPermutation(int setNumber, int numberOfObjects, boolean repeated, boolean order, BigInteger answer) {
		assertEquals(answer, perm.number(setNumber, numberOfObjects, repeated, order));
	}

	@Test(expected = IllegalArgumentException.class)
	@Parameters(method="InValidPermutation")
	public void testInvalidPermutation(int setNumber, int numberOfObjects, boolean repeated, boolean order) {
		perm.number(setNumber, numberOfObjects, repeated, order);
	}
	
}




