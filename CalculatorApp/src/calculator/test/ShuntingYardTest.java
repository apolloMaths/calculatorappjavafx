package calculator.test;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import calculator.MainApp;
import calculator.exceptions.DivideByZeroException;
import calculator.util.ShuntingYardAlgorithm;
import javafx.application.Application;
import javafx.embed.swing.JFXPanel;

public class ShuntingYardTest {

	private ShuntingYardAlgorithm algo = new ShuntingYardAlgorithm();
	@Test
	public void testAllFunctions() {
		assertEquals(Double.parseDouble(algo.allFunctions("cos ( 30 ) + sin ( 30 ) + tan ( 30 )", false)),-7.23911137085,2);
		assertEquals(Double.parseDouble(algo.allFunctions("ln ( 5 ) + exp ( 9 ) + log ( 5 ) + sqrt ( 9 )", false)),8108.39233549,2);
	}

	@Test
	public void testNegative(){
		assertEquals(Double.parseDouble(algo.allFunctions("9 - -9", true)),18.0,0);
	}

	@Test
	public void testOperators(){
		assertEquals(Double.parseDouble(algo.allFunctions("9 / 9", true)),1,0);
		assertEquals(Double.parseDouble(algo.allFunctions("9 ^ 2", true)),81.0,0);
		assertEquals(Double.parseDouble(algo.allFunctions("9 * 9", true)),81.0,0);
	}

	@Test
	public void testDegrees(){
		assertEquals(Double.parseDouble(algo.allFunctions("cos ( 30 ) + sin ( 30 ) + tan ( 30 )", true)),0.0,2);
	}

	@Test
	public void testMinusAtStart(){
		assertEquals(Double.parseDouble(algo.allFunctions("-9 - 9", true)),-18.0,0);
	}

	@Test
	public void testPrecedent(){
		assertEquals(Double.parseDouble(algo.allFunctions(" 1 + ( 6 + 7 ) ", true)),-18.0,0);
	}

}
