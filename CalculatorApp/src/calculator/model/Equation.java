package calculator.model;

import javafx.beans.property.SimpleStringProperty;

public class Equation {
	private SimpleStringProperty equationName;
	private SimpleStringProperty equation;

	public Equation(String nameParam,String equationParam){
		this.equationName = new SimpleStringProperty(nameParam);
		this.equation = new SimpleStringProperty(equationParam);;
	}
	public void setEquationName(SimpleStringProperty equationName) {
		this.equationName = equationName;
	}
	public void setEquation(SimpleStringProperty equation) {
		this.equation = equation;
	}

	public SimpleStringProperty equationNameProperty(){
	    return equationName;
	}

	public SimpleStringProperty equationProperty(){
	    return equation;
	}

	public String getName() {
        return equationName.getValue();
    }

	public String getEquation() {
        return equation.getValue();
    }



}
