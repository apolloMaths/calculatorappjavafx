package calculator.util;

import java.text.DecimalFormat;

public class HaarDecompositionRecomposition {
	static int size=8;
	static double [][] arrayParam={{230,127,127,200,200,127,127,230},
									{127,230,127,230,230,127,230,127},{127,127,230,230,230,230,127,127},{200,230,230,255,255,230,230,230},
									{200,230,230,255,255,230,230,230},{127,127,230,230,230,230,127,127},{127,230,127,230,230,127,230,127},
									{230,127,127,200,200,127,127,230}};
	static double [][] tempArray=new double[size][size];
	static DecimalFormat df=new DecimalFormat("####0.00");

	public static void main(String[] args){
		printMatrix(arrayParam);
		double[][] hello={{230,127,127,200,200,127,127,230},
				{127,230,127,230,230,127,230,127},{127,127,230,230,230,230,127,127},{200,230,230,255,255,230,230,230},
				{200,230,230,255,255,230,230,230},{127,127,230,230,230,230,127,127},{127,230,127,230,230,127,230,127},
				{230,127,127,200,200,127,127,230}};
		hello = decompose(hello);
		hello = recompose(hello);
	}
	public static double[][] decompose(double [][]arrayParam){
		System.out.println();
		System.out.println("*********************Decomposition on Rows***************************");
		int index=size;
		int j,k;
		int offset;
		int step=1;
		while(index>=2){ //decompositon on rows
			offset=(index/2);
			for(k=0;k<size;k++){
				for(j=0;j<index;j+=2){
					double avg=(arrayParam[k][j] + arrayParam [k][j+1])/2.0;
					double diff=arrayParam[k][j] - avg;
					//put these elements in temp array
					tempArray [k] [j/2]=avg;
					tempArray [k] [offset + (j/2)]=diff;
				}

			}
			// Store decomposed data in pixelarray for next decomposition step.


			for(k=0;k<size;k++)
				for(j=0;j<index;j++)
					arrayParam[k][j] = tempArray[k][j];

			index/=2;
			step++;
		}
		printMatrix(arrayParam); // rows decomposition

		System.out.println();
		System.out.println("*******************Decomposition on Columns*************************");
		index=size;
		step=1;
		while(index>=2){
			offset=(index/2);
			for(j=0;j<size;j++){
				for(k=0;k<index;k+=2){
					double avg=(tempArray[k] [j] + tempArray [k+1] [j])/2.0;
					double diff=tempArray[k] [j] - avg;
					arrayParam [k/2] [j]=avg;
					arrayParam [offset + (k/2)] [j]=diff;
				}
			}
			for(j=0;j<size;j++)
				for(k=0;k<index;k++)
				tempArray[k][j] = arrayParam[k][j];
			index/=2;
			step++;
		}
		printMatrix(arrayParam);
		return arrayParam;
	}



	public static double[] [] recompose(double[][] arrayParam){
		System.out.println();
		System.out.println("************************Recomposition on coulmns*********************");
		 int index = 1;
	     int step = 1;

	     while (index < size)
	     {
	       for ( int j = 0; j < size; j++)
	       {
	         for (int i = 0; i < index; i++)
	         {
	           arrayParam[(2 * i)][j] = (tempArray[i][j] + tempArray[(index + i)][j]);
	           arrayParam[(2 * i + 1)][j] = (tempArray[i][j] - tempArray[(index + i)][j]);
	         }
	       }

	       index *= 2;
	       for ( int j = 0; j < size; j++)
	       for ( int k = 0; k < index; k++)
	       tempArray[k][j] = arrayParam[k][j];

	       step++;
	     }
	     printMatrix(arrayParam);   //columns recomposition
	     index=1;
	     step=1;
	     System.out.println();
	     System.out.println("************************Recomposition on Rows*********************");
	     while (index < size)
	      {
	        for (int i = 0; i < size; i++)
	        {
	          for ( int j = 0; j < index; j++)
	          {
	            tempArray[i][(2 * j)] = (arrayParam[i][j] + arrayParam[i][(index + j)]);
	            tempArray[i][(2 * j + 1)] = (arrayParam[i][j] - arrayParam[i][(index + j)]);
	          }
	        }

	       index *= 2;
	        for (int k = 0; k < size; k++)
	        for (int j = 0; j < index; j++)
	        arrayParam[k][j] = tempArray[k][j];
	        step++;
	      }
		printMatrix(arrayParam);  // returns original matrix  rows recomposition
		return arrayParam;
	}

	static void printMatrix(double[][] result){
		System.out.println();
		for (int i = 0; i < result.length; i++) {
			for (int j = 0; j < result[0].length; j++) {
				System.out.print((int)result[i][j]+"\t"); // remove int for double numbers

			}
			System.out.println(" ");

		}
	}

}
