/*
 * When the order doesn't matter, it is a Combination.
 * When the order does matter it is a Permutation
 */

package calculator.util;

import java.math.BigInteger;
import java.text.NumberFormat;

public class Permutations {

	static BigInteger factorialFactory = new BigInteger("1");
	static NumberFormat numberFormat;
	static BigInteger answer=new BigInteger("0");
	static double size=0d;


	public static BigInteger number(int setNumber, int numberOfObjects, boolean repeated, boolean order) {
		if (setNumber<0 | numberOfObjects<0) {
			throw new IllegalArgumentException("Cannot have negative values");
		} else {
		// equation n^r when rep is allowed and order is important
		if (repeated & order){
			answer = BigInteger.valueOf(setNumber).pow(numberOfObjects);
			//			System.out.println("Permutation with repititon "+(int)answer);
			// equation n! / (n-r)! when no reps and there is order
		} else if (!repeated & order) {
			if(numberOfObjects>setNumber) {
				throw new IllegalArgumentException("Number chosen is bigger than set given");
			} else {
				answer = factorialFactory(BigInteger.valueOf(setNumber)).divide(factorialFactory(BigInteger.valueOf(setNumber).subtract(BigInteger.valueOf(numberOfObjects))));
				System.out.println("Permutation without repition: "+answer);
			}
			// equation= (r+n-1)! / r! * (n-r)! when rep allowed and order is not important
		} else if (repeated & !order) {
				answer = factorialFactory(BigInteger.valueOf(setNumber).add(BigInteger.valueOf(numberOfObjects)).subtract(BigInteger.valueOf(1))).divide((factorialFactory(BigInteger.valueOf(numberOfObjects)).multiply(factorialFactory(BigInteger.valueOf(setNumber).subtract(BigInteger.valueOf(1))))));
				System.out.println("Combination with repitition "+answer);
				// equation = n! / (r! * (n-r)!) when no reps and order is not important
			} else if (!repeated & !order) {
				if(numberOfObjects>setNumber) {
					throw new IllegalArgumentException("Number chosen is bigger than set given");

				} else {
					answer = factorialFactory(BigInteger.valueOf(setNumber)).divide((factorialFactory(BigInteger.valueOf(numberOfObjects)).multiply(factorialFactory(BigInteger.valueOf(setNumber).subtract(BigInteger.valueOf(numberOfObjects))))));
					System.out.println("Combination without repitition "+answer);
				}
			}
		}
			return answer;
		}

	public static String toStringNum(BigInteger answer) {
		
		numberFormat = NumberFormat.getInstance();
		//allows for whole numbers
		numberFormat.setMinimumFractionDigits(0);
		//max of 3 decimal places


		String resultString = "";
		String decimalValue = "";

		decimalValue = numberFormat.format(answer);
		resultString = resultString.concat(decimalValue + "  ");

		return resultString;
	}
	
	static BigInteger factorialFactory(BigInteger number) {
		factorialFactory = new BigInteger("1");
		if (number == new BigInteger("0")) {
			return new BigInteger("1");
		} else {
			for (int i = 1; i <= number.intValue(); i++) {
				factorialFactory = factorialFactory.multiply(BigInteger.valueOf(i));
			}
			return factorialFactory;
		}
	}
	
/**********************************************************************************************************************************************************/	
	public static int possibleStrings(int maxLength, char[] textLetters, String currentString) {
//		size = 0d;
		// If the current string has reached it's maximum length
//		System.out.println("CurrentString:" + currentString);
		if(currentString.length() == maxLength) {
						System.out.println(currentString);
			size++;
			// Else add each letter from the alphabet to new strings and process these new strings again
		} else {
			for(int i = 0; i < textLetters.length; i++) {
				String oldCurr = currentString;
				currentString += textLetters[i];
				possibleStrings(maxLength,textLetters,currentString);
				currentString = oldCurr;
			}
		}
		return (int) size;
	}
	
	


}
