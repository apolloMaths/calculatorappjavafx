
package calculator.util;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.StringTokenizer;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class Matrices {

	static double matrixA[][] = {{2,3},{3,2}};
	static double matrixB[][] = {{2,4},{5,2}};
	static double matrixA1[][] = {{2,3,6},{3,2,8}};
	static double matrixB1[][] = {{2,4,4},{5,2,2}};
	static double matrixMA[][] = {{2,3,6},{3,2,8}};
	static double matrixMB[][] = {{2,4},{5,2}};
	static String inputMatrix = "1 2\n4 5";
	static NumberFormat numberFormat;
	static int maxDecimals = 3;
	static int numRowsA = 2;
	static int numRowsMA = 2;
	static int numColumnsA = 2;
	static int numColumnsMA = 3;
	static int numRowsB = 2;
	static int numRowsMB = 2;
	static int numColumnsMB = 2;
	static int numColumnsB = 2;





	public static void main(String[] args) {

		//Test 1 - method Add
		double result1[][] = Add(matrixA1,matrixB1, numRowsA, numColumnsA, numRowsB, numColumnsB);
		//expected result = {{4.0,7.0},{8.0,4.0}}; - passed
		System.out.println("Adding Result:" + Arrays.deepToString(result1));

//		//Test 2 - method aSubtractB
//		double result2[][] = AsubtractB(matrixA1,matrixB1, numRowsA, numColumnsA, numRowsB, numColumnsB);
//		//expected result = {{0.0,-1.0},{-2.0,0.0}}; - passed
//		System.out.println("Subtracting A - B Result:" + Arrays.deepToString(result2));

//		//Test 3 - method bSubtractA
//		double result3[][] = BsubtractA(matrixA1,matrixB1, numRowsA, numColumnsA, numRowsB, numColumnsB);
//		//expected result = {{0.0,1.0},{2.0,0.0}}; - passed
//		System.out.println("Subtracting B - A Result:" + Arrays.deepToString(result3));

		//Test 4 - method Multiply
		double result4[][] = Multiply(matrixMA,matrixMB, numRowsMA, numColumnsMA, numRowsMB, numColumnsMB);
		//expected result = {{19.0,14.0},{16.0,16.0}}; - passed
		System.out.println("Multiplication Result:" + Arrays.deepToString(result4));

		//Test 5 - method Transpose
		double result5[][] = Transpose(matrixA, numRowsA, numColumnsA);
		//expected result = {{2.0,3.0},{3.0,2.0}}; - passed
		System.out.println("Transpose A Result:" + Arrays.deepToString(result5));

		//Test 6 - method Transpose
		double result6[][] = Transpose(matrixB, numRowsB, numColumnsB);
		//expected result = {{2.0,5.0},{4.0,2.0}}; - passed
		System.out.println("Transpose B Result:" + Arrays.deepToString(result6));

		//Test 7 - method DeterminantA
		double resultD1 = Determinant(matrixA, numRowsA, numColumnsA);
		//expected result = -5 - passed
		System.out.println("Determinant A Result:" + resultD1);

		//Test 8 - method DeterminantB
		double resultD2 = Determinant(matrixB, numRowsA, numColumnsA);
		//expected result = -16 - passed
		System.out.println("Determinant B Result:" + resultD2);

		//Test 8 - method Multiply MatrixA by X
		double result7[][] = MultiplyMatrixbyX(matrixA, 2, numRowsA, numColumnsA);
		//expected result = {{4.0,6.0},{6.0,4.0}}; - passed
		System.out.println("MultiplyA by X Result:" + Arrays.deepToString(result7));

		//Test 8 - method Multiply MatrixB by X
		double result8[][] = MultiplyMatrixbyX(matrixB, 2, numRowsB, numColumnsB);
		//expected result = {{4.0,8.0},{10.0,4.0}}; - passed
		System.out.println("MultiplyB by X Result:" + Arrays.deepToString(result8));

//		//Test 9 - method to parse inputMatrix string to matrix[][]
//		double result9[][] = ReadInMatrices(inputMatrix, numRowsA, numColumnsA, numRowsB, numColumnsB);
//		//expected result = 1 2 4 5 - passed
//		System.out.println("ReadInMatrices Result:" + Arrays.deepToString(result9));

		//Test 10 - method to parse inputMatrix string to matrix[][]
		String result10 = Display(matrixA1, numRowsA, numColumnsA);
		//expected result = 2.0 3.0\n3.0 2.0 - passed
		System.out.println("Display matrixA Result:\n" + result10);

		//Test 11 - method to parse inputMatrix string to matrix[][]
		String result11 = Display(matrixB1, numRowsA, numColumnsA);
		//expected result = 2.0 4.0\n5.0 2.0 - passed
		System.out.println("Display matrixB Result:\n" + result11);


	}

	//Add matrixA and matrixB
	public static double[][] Add(double[][] a, double[][] b, int rowsA, int columnsA, int rowsB, int columnsB)
	{		
		double matrix[][] = new double[rowsA][columnsA];

		if(rowsA != rowsB){
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error Message");
			alert.setHeaderText("Matrix Addition:");
			alert.setContentText("The number of rows in Matrix A must be equal to the number of rows in Matrix B!");
			alert.showAndWait();
		}else if(columnsA != columnsB){
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error Message");
			alert.setHeaderText("Matrix Addition:");
			alert.setContentText("The number of columns in Matrix A must match the number of columns in Matrix B!");
			alert.showAndWait();
		}else{
			for (int r=0; r < rowsA; r++){

				for (int c=0; c < columnsA; c++)
				{
					matrix[r][c] = a[r][c] + b[r][c];
				}
			}
		}
			return matrix;
		}

		//Subtract matrixA from matrixB
		public static double[][] AsubtractB(double[][] a, double[][] b, int rowsA, int columnsA, int rowsB, int columnsB)
		{
			int matrixSizeA = a.length;
			int matrixSizeB = b.length;
			//checks that both matrices are the same size
			//		if (matrixSizeA != matrixSizeB){
			//			System.out.println("Matrices must be the same size!");
			//		}

			double matrix[][] = new double[rowsA][columnsA];

			if(rowsA != rowsB){
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Error Message");
				alert.setHeaderText("Matrix Subtraction:");
				alert.setContentText("The number of rows in Matrix A must be equal to the number of rows in Matrix B!");
				alert.showAndWait();
			}else if(columnsA != columnsB){
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Error Message");
				alert.setHeaderText("Matrix Subtraction:");
				alert.setContentText("The number of columns in Matrix A must match the number of columns in Matrix B!");
				alert.showAndWait();
			}else{
			
			for(int r=0; r<rowsA; r++)
			{
				for(int c=0; c<columnsA; c++)
				{
					matrix[r][c] = a[r][c] - b[r][c];
				}
			}
			}

			return matrix;
		}

		//Subtract matrixB from matrixA
		public static double[][] BsubtractA(double[][] a, double[][] b, int rowsA, int columnsA, int rowsB, int columnsB)
		{
			int matrixSizeA = a.length;
			int matrixSizeB = b.length;
			//checks that both matrices are the same size
//			if (matrixSizeA != matrixSizeB){
//				System.out.println("Matrices must be the same size!");
//			}

			double matrix[][] = new double[rowsA][columnsA];

			if(rowsA != rowsB){
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Error Message");
				alert.setHeaderText("Matrix Subtraction:");
				alert.setContentText("The number of rows in Matrix A must be equal to the number of rows in Matrix B!");
				alert.showAndWait();
			}else if(columnsA != columnsB){
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Error Message");
				alert.setHeaderText("Matrix Subtraction:");
				alert.setContentText("The number of columns in Matrix A must match the number of columns in Matrix B!");
				alert.showAndWait();
			}else{

			for(int r=0; r<rowsA; r++)
			{
				for(int c=0; c<columnsA; c++)
				{
					matrix[r][c] = b[r][c] - a[r][c];
				}
			}
			
			}

			return matrix;
		}

		//Multiply matrixA and matrixB - need to test this!!
		public static double[][] Multiply(double[][] a, double[][] b, int rowsA, int columnsA,int rowsB, int columnsB )
		{
			int matrixSizeA = a.length;
			int matrixSizeB = b.length;
			//checks that both matrices are the same size
			//		if (matrixSizeA != matrixSizeB){
			//			Alert alert = new Alert(AlertType.ERROR);
			//			alert.setTitle("Error Message");
			//			alert.setHeaderText("Invalid Matrix Entered - must be a square matrix!");
			//			alert.showAndWait();
			//		}

			double matrix[][] = new double[columnsB][rowsA];

			if(rowsA != columnsB){
				System.out.println("RowsA != ColumnsB ");
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Error Message");
				alert.setHeaderText("Matrix Multiplication:");
				alert.setContentText("The number of rows in Matrix A must be equal to the number of columns in Matrix B!");
				alert.showAndWait();
			}else{
			System.out.println("RowsA == ColumnsB");
			int sum = 0;
			for(int r=0; r<rowsA; r++)
			{
				for(int c=0; c<columnsB; c++)
				{
					sum=0;
					for(int k=0; k<rowsA; k++)
					{
						sum+=a[r][k]*b[k][c];
					}    
					matrix[r][c]=sum;
				}
			}
			}
			return matrix;
		}

		//Transpose matrices - currently must be square
		public static double[][] Transpose(double[][] inputMatrix, int numRows, int numColumns){

			
			
			int matrixSizeA = inputMatrix.length;

			double matrix[][] = new double[numRows][numColumns];

			if(numRows != numColumns){
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Error Message");
				alert.setHeaderText("Matrix Transpose:");
				alert.setContentText("Matrix must be square!");
				alert.showAndWait();
			}
			
			for ( int r = 0 ; r < numRows; r++ )
			{
				for (int c = 0 ; c < numColumns; c++ )               
					matrix[c][r] = inputMatrix[r][c];
			}
			return matrix;
		}


		//Determinant of the matrix
		public static double Determinant(double[][] inputMatrix, int numRows, int numColumns)
		{

			if(numRows != numColumns){
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Error Message");
				alert.setHeaderText("Matrix Determinant:");
				alert.setContentText("Matrix must be square!");
				alert.showAndWait();
			}
			int matrixSize = inputMatrix.length;

			double det=0;
			if(matrixSize == 1)
			{
				det = inputMatrix[0][0];
			}
			else if (matrixSize == 2)
			{
				det = inputMatrix[0][0]*inputMatrix[1][1] - inputMatrix[1][0]*inputMatrix[0][1];
			}
			else
			{
				det=0;
				for(int j1=0;j1<matrixSize;j1++)
				{
					double[][] matrix = new double[matrixSize-1][];
					for(int k=0;k<(matrixSize-1);k++)
					{
						matrix[k] = new double[matrixSize-1];
					}
					for(int i=1;i<matrixSize;i++)
					{
						int j2=0;
						for(int j=0;j<matrixSize;j++)
						{
							if(j == j1)
								continue;
							matrix[i-1][j2] = inputMatrix[i][j];
							j2++;
						}
					}
					det += Math.pow(-1.0,1.0+j1+1.0)* inputMatrix[0][j1] * Determinant(matrix, numRows, numColumns);
				}
			}
			return det;
		}

		//Multiply matrix by X
		public static double[][] MultiplyMatrixbyX(double[][] inputMatrix, double multiplyBy, int numRows, int numColumns)
		{
			int matrixSize = inputMatrix.length;
			double[][] matrix = new double[numRows][numColumns];

			for (int i = 0; i < numRows; i++) { 
				for (int j = 0; j < numColumns; j++) { 
					matrix[i][j] = inputMatrix[i][j] * multiplyBy;
				}
			}

			return matrix;
		}

		//right now only works if both matrices are square or if matrixA is long vs wide
		//need to maybe say rowsA need to be equal to columnsB and matrixB needs to be square!
		//Divide matrix A by matrix B -- ((matrixA)x((matrixB)^-1))
		public static double[][] DivideA(double[][] a, double[][] b, int rowsA, int columnsA, int rowsB, int columnsB)
		{
			
			if((rowsA != columnsA) || (rowsB != columnsB)){
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Error Message");
				alert.setHeaderText("Matrix Division:");
				alert.setContentText("Both matrices must be square!");
				alert.showAndWait();
			}
				double [][] invMatrixB = Inverse(b, rowsB, columnsB);
		 
		 		int matrixSizeA = a.length;
		 		int matrixSizeB = b.length;
		 		
		 
		 		double[][] matrix = new double[matrixSizeA][matrixSizeA];
		
		
				int sum = 0;
		 		for(int r=0; r<matrixSizeA; r++)
		 		{
		 			for(int c=0; c<matrixSizeA; c++)
		 			{
					matrix[r][c] = matrixA[r][c]/matrixB[r][c];
						sum=0;
					for(int k=0; k<matrixSizeA; k++)
						{
							sum+= (invMatrixB[k][c]) * a[r][k];
						}    
						matrix[r][c]=sum;
		 			}
		 		}
		 		return matrix;
		 	}

		//Divide matrix B by matrix A -- ((matrixB)x((matrixA)^-1))
//		public static double[][] DivideB(double[][] a, double[][] b, int numRows, int numColumns)
//		{
//			double [][] invMatrixA = Inverse(a, numRows, numColumns);
//
//			int matrixSizeA = a.length;
//			int matrixSizeB = b.length;
//			//checks that both matrices are the same size
//			if (matrixSizeA != matrixSizeB){
//				System.out.println("Matrices must be the same size!");
//			}
//
//			double[][] matrix = new double[matrixSizeA][matrixSizeA];
//			int sum = 0;
//			for(int r=0; r<matrixSizeA; r++)
//			{
//				for(int c=0; c<matrixSizeA; c++)
//				{
//					sum=0;
//					for(int k=0; k<matrixSizeA; k++)
//					{
//						sum+= (invMatrixA[k][c]) * b[r][k];
//					}    
//					matrix[r][c]=sum;
//				}
//			}
//			return matrix;
//		}

		//need to test
		//Multiply matrix by X
		public static double[][] DivideMatrixbyX(double[][] inputMatrix, double multiplyBy, int numRows, int numColumns)
		{
			int matrixSize = inputMatrix.length;
			double[][] matrix = new double[numRows][numColumns];

			for (int i = 0; i < numRows; i++) { 
				for (int j = 0; j < numColumns; j++) { 
					matrix[i][j] = inputMatrix[i][j] / multiplyBy;
				}
			}

			return matrix;
		}

		//not throwing an error when matrices are not square!!!
		//method to get the inverse of a matrix
		public static double[][] Inverse(double[][] a, int numRows, int numColumns)
		{
			
			if(numRows != numColumns){
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Error Message");
				alert.setHeaderText("Matrix Inverse:");
				alert.setContentText("Matrices must be square!");
			}
			
			// Formula used to Calculate Inverse:
			// inv(A) = 1/det(A) * adj(A)
			double matrix[][] = new double[numRows][numColumns];
			
			
				int matrixSize = a.length;
				
			double emptyMatrix[][] = new double[matrixSize][matrixSize];
			//adjoint matrix
			double mm[][] =  Adjoint(a, numRows, numColumns);

			//determinant value
			double det = Determinant(a, numRows, numColumns);

			double dd = 0;

			if (det == 0) 
			{ 
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Error Message");
				alert.setHeaderText("Determinant is zero!");
				alert.setContentText("The matrix is not invertible!");

				alert.showAndWait();
				return emptyMatrix;
			}
			else
			{
				dd = 1/det;
			}

			for (int i=0; i < matrixSize; i++){
				for (int j=0; j < matrixSize; j++){

					matrix[i][j] = dd * mm[i][j];
				}
			}
			
			return matrix;
		}


		//i think this may actually be the same as the transpose CHECK THIS!!
		//swap values on the diagonal from right to left...change the sign of the values from left to right
		public static double[][] Adjoint(double[][] a, int numRows, int numColumns) 
		{
			int matrixSize = a.length;

			double matrix[][] = new double[matrixSize][matrixSize];

			if(numRows != numColumns){
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Error Message");
				alert.setHeaderText("Adjoint matrix:");
				alert.setContentText("The matrix must be square!");

				alert.showAndWait();
			}else{
			int ii;
			int jj;
			int ia;
			int ja;
			double det;

			for (int i=0; i < matrixSize; i++)
				for (int j=0; j < matrixSize; j++)
				{
					ia = ja = 0;

					double ap[][] = new double[matrixSize-1][matrixSize-1];

					for (ii=0; ii < matrixSize; ii++)
					{
						for (jj=0; jj < matrixSize; jj++)
						{

							if ((ii != i) && (jj != j))
							{
								ap[ia][ja] = a[ii][jj];
								ja++;
							}

						}
						if ((ii != i ) && (jj != j)){
							ia++;
						} 
						ja=0; 
					}

				det = Determinant(ap, numRows, numColumns);
					matrix[i][j] = (double)Math.pow(-1, i+j) * det;
				}

			matrix = Transpose(matrix, numRows, numColumns);
			}
			return matrix;
		}


		//method to read in the matrix string
		public static double[][] ReadInMatrices(String inputMatrix, int numRows, int numColumns) 
		{

			int maxSize = 100;
			String value = "";
			int i=0;
			int j=0;
			int[] tempMatrix = new int[maxSize];

			//parse the input text to get the matrix size and check its square/valid
			StringTokenizer ts = new StringTokenizer(inputMatrix, "\n");
			while (ts.hasMoreTokens()) 
			{
				StringTokenizer ts2 = new StringTokenizer(ts.nextToken());
				while (ts2.hasMoreTokens()) {
					ts2.nextToken();
					j++; 
				}

				tempMatrix[i] = j;
				i++;
				j=0;
			}

			//checking the number of columns equal the number of rows i.e square matrix
			//		for (int c=0; c < i; c++)
			//		{     
			//			if (tempMatrix[c] != i) { 
			//				
			//				 
			//			}
			//		}

			//sets the matrix size based on the tokens read in above
			int n = i;

			//creating the matrix 
			double matrix[][] = new double[numRows][numColumns];

			//you are here
			i = j = 0;
			value="";

			//parsing the input text to create the matrix
			StringTokenizer st = new StringTokenizer(inputMatrix, "\n");
			while (st.hasMoreTokens()) 
			{
				StringTokenizer st2 = new StringTokenizer(st.nextToken());
				while (st2.hasMoreTokens())
				{
					value = st2.nextToken();
					matrix[i][j] = Double.valueOf(value).doubleValue();

					j++;
				}
				i++; 
				j=0;
			}

			return matrix;
		}
		
		//may need to use this to display multiplied matrix result
		public static String DisplayMult(double[][] matrix, int rowsA, int columnsB)
		{
			numberFormat = NumberFormat.getInstance();
			//allows for whole numbers
			numberFormat.setMinimumFractionDigits(0);
			//max of 3 decimal places
			numberFormat.setMaximumFractionDigits(maxDecimals); 

			int matrixSize = matrix.length;

			String resultString = "";
			String decimalValue = "";

			//iterates through the rows and columns
			for (int i=0; i < rowsA; i++)
			{
				for (int j=0; j < columnsB; j++)
				{
					decimalValue = numberFormat.format(matrix[i][j]);
					resultString = resultString.concat(decimalValue + "  ");
				}

				resultString = resultString.concat("\n");
			}

			return resultString;
		}
		
		
		
		

		//method to display the resulting matrix for subtracting and adding
		public static String Display(double[][] matrix, int numRows, int numColumns)
		{
			numberFormat = NumberFormat.getInstance();
			//allows for whole numbers
			numberFormat.setMinimumFractionDigits(0);
			//max of 3 decimal places
			numberFormat.setMaximumFractionDigits(maxDecimals); 

			int matrixSize = matrix.length;

			String resultString = "";
			String decimalValue = "";

			//iterates through the rows and columns
			for (int i=0; i < numRows; i++)
			{
				for (int j=0; j < numColumns; j++)
				{
					decimalValue = numberFormat.format(matrix[i][j]);
					resultString = resultString.concat(decimalValue + "  ");
				}

				resultString = resultString.concat("\n");
			}

			return resultString;
		}
		
		//may need to use this to display the transpose matrix....the num of columns and rows of the resulting matrix are different (doesnt work right now)
				public static String DisplayTranspose(double[][] matrix, int numRowsA, int numColumnsA)
				{
					numberFormat = NumberFormat.getInstance();
					//allows for whole numbers
					numberFormat.setMinimumFractionDigits(0);
					//max of 3 decimal places
					numberFormat.setMaximumFractionDigits(maxDecimals); 

					int matrixSize = matrix.length;

					String resultString = "";
					String decimalValue = "";

					//iterates through the rows and columns
					for (int i=0; i < numRowsA; i++)
					{
						for (int j=0; j < numColumnsA; j++)
						{
							decimalValue = numberFormat.format(matrix[i][j]);
							resultString = resultString.concat(decimalValue + "  ");
						}

						resultString = resultString.concat("\n");
					}

					return resultString;
				}
	
	}
