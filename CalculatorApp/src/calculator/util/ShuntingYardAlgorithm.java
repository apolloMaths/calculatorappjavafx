package calculator.util;
import java.util.Stack;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;


public class ShuntingYardAlgorithm {

	static String ops[] = {"+","-","*","/","^","sqrt","cos","sin","tan","log","ln","exp","(",")"};
	//static String ops[] = {"+","-","*","/","(",")"};
	public static String allFunctions(String inputParam,boolean RadiansParam) {
		String result = inputParam;
		System.out.println(result);
		if(result.charAt(0) == '-'){
			result = "0 "+result;
			System.out.println("Beginning was zero: "+result);
		}
		//put if statement to handle first value being a negative number,fixes everything
//		for(String operator:ops){
//			result = formatEquation(result,operator);
//			System.out.println("Result of " +operator + ":" + result);
//		}
		System.out.println("Result format is " + result);
		result = removeSqrt(result,RadiansParam);
		result = removeCos(result,RadiansParam);
		result = removeSin(result,RadiansParam);
		result = removeTan(result,RadiansParam);
		result = removeExp(result,RadiansParam);
		result = removeLn(result,RadiansParam);
		result = removeLog(result,RadiansParam);
		result = infixToPostfix(result);
		System.out.println("Result postfix is " + result);
		result = calculatePostfix(result,RadiansParam);
		//result = RadOrDeg(result,RadiansParam);
		System.out.println("Result : "+ result);
		return result;
	}

	/*public static String RadOrDeg(String inputParam,boolean radianParam){
		if(radianParam){
			System.out.println("RADORDEG: Degrees");
			return Double.toString(Math.toDegrees(Double.parseDouble(inputParam)));
		}
		else{
			System.out.println("RADORDEG: Radians");
			return Double.toString(Math.toRadians(Double.parseDouble(inputParam)));
		}

	}*/
//	static String formatEquation(String inputParam,String operator) {
//		StringBuilder result = new StringBuilder();
//		String loopString = inputParam;
//		while(loopString.contains(operator)){
//			int index = loopString.indexOf(operator);
//			//if not the first part of the equation
//			if(index != 0){
//				//replace space before operator
//				if(loopString.charAt(index-1) != ' '){
//					result.append(loopString.substring(0, index));
//					result.append(" ");
//					result.append(operator);
//				}
//				else{
//					result.append(loopString.substring(0, index));
//					result.append(operator);
//				}
//			}
//			else{
//				result.append(operator);
//			}
//			//Adds space after operator
//			if(index != loopString.length()){
//				if(loopString.charAt(index+operator.length()-1) != ' '){
//					result.append(" ");
//				}
//			}
//			//Changeover to rest of string
//			loopString = loopString.substring(index+operator.length(),loopString.length());
//		}
//		result.append(loopString);
//		return result.toString();
//	}
	static String infixToPostfix(String infix) {
		/*
		 * To find out the precedence, we take the index of the token in the ops string
		 * and divide by 2 (rounding down). This will give us: 0, 0, 1, 1, 2
		 */
		final String operators = "-+/*^";

		StringBuilder sb = new StringBuilder();
		Stack<Integer> s = new Stack<>();

		for (String token : infix.split("\\s")) {
//			if (token.isEmpty())
//				continue;
			char c = token.charAt(0);
			int idx = operators.indexOf(c);
			System.out.println(" char: "+ sb.toString());
			if(token.length() > 1 && c =='-'){
				sb.append(token).append(' ');
			}
			// check for operator
			else if (idx != -1) {
				if (s.isEmpty())
					s.push(idx);

				else {
					while (!s.isEmpty()) {
						int prec2 = s.peek() / 2;
						int prec1 = idx / 2;
						if (prec2 > prec1 || (prec2 == prec1 && c != '^'))
							sb.append(operators.charAt(s.pop())).append(' ');
						else
							break;
					}
					s.push(idx);
				}
			} else if (c == '(') {
				s.push(-2); // -2 stands for '('
			} else if (c == ')') {
				// until '(' on stack, pop operators.
				while (s.peek() != -2)
					sb.append(operators.charAt(s.pop())).append(' ');
				s.pop();
			} else {
				sb.append(token).append(' ');
			}
		}
		while (!s.isEmpty())
			sb.append(operators.charAt(s.pop())).append(' ');
		return sb.toString();
	}

	static String calculatePostfix(String postfixParam,boolean RadiansParam) {
		postfixParam = removeCos(postfixParam,RadiansParam);
		postfixParam = removeSin(postfixParam,RadiansParam);
		postfixParam = removeTan(postfixParam,RadiansParam);
		postfixParam = removeExp(postfixParam,RadiansParam);
		postfixParam = removeLn(postfixParam,RadiansParam);
		postfixParam = removeLog(postfixParam,RadiansParam);
		//postfixParam = infixToPostfix(postfixParam);
		Stack<String> numberStack = new Stack<String>();
		for (String token : postfixParam.split("\\s")) {
			if (token.equals("+")) {
				//System.out.println("Addition: ");
				double secondValue = Double.parseDouble(numberStack.pop());
				double firstValue = Double.parseDouble(numberStack.pop());
				double result = firstValue + secondValue;
				numberStack.push(Double.toString(result));

			} else if (token.equals("-")) {
				double result = 0;
				double secondValue = 0;
				try{
					secondValue = Double.parseDouble(numberStack.pop());
					double firstValue = Double.parseDouble(numberStack.pop());
					result = firstValue - secondValue;
				}
				catch(Exception e){
					numberStack.push(Double.toString(secondValue));
					double firstValue = Double.parseDouble(numberStack.pop());
					result = firstValue;
				}
				numberStack.push(Double.toString(result));
			} else if (token.equals("/")) {
				//System.out.println("Division: ");
				double secondValue = Double.parseDouble(numberStack.pop());
				if(secondValue == 0){
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("Error");
					alert.setHeaderText("Division Error");
					alert.setContentText("Attempted to divide by zero");
					alert.showAndWait();
					return null;
					//System.out.println("Divided by zero:");
					//throw new DivideByZeroException("DividedByZero");
				}
				double firstValue = Double.parseDouble(numberStack.pop());
				double result = firstValue / secondValue;
				numberStack.push(Double.toString(result));
			} else if (token.equals("*")) {
				double result = 0;
				double secondValue = 0;
				try{
					secondValue = Double.parseDouble(numberStack.pop());
					double firstValue = Double.parseDouble(numberStack.pop());
					result = firstValue * secondValue;
				}
				catch(Exception e){
					numberStack.push(Double.toString(secondValue));
					double firstValue = Double.parseDouble(numberStack.pop());
					System.out.println("FirstValue:" + firstValue);
					result = firstValue * -1;
				}
				numberStack.push(Double.toString(result));
			} else if (token.equals("^")) {
				//System.out.println("Power : ");
				double secondValue = Double.parseDouble(numberStack.pop());
				double firstValue = Double.parseDouble(numberStack.pop());
				double result = Math.pow(firstValue, secondValue);
				numberStack.push(Double.toString(result));
			} else {
				//System.out.println(token);
				numberStack.push(token);
			}
		}
		return numberStack.pop();

	}

//	static String removeUnary(String inputParam) {
//		StringBuilder sb = new StringBuilder();
//		//Run through String until it reaches a negative number and inserts 0 - before that number
//		for(int i =0;i<inputParam.length();i++) {
//			if((inputParam.charAt(i)=='-') && (Character.isDigit(inputParam.charAt(i+1)))) {
//				sb.append(" 0 - ");
//			}
//			else
//			{
//				sb.append(inputParam.charAt(i));
//			}
//		}
//		System.out.println("Result of unary:" + sb.toString());
//		return sb.toString();
//	}

	static String removeSqrt(String inputParam,boolean RadiansParam) {
		int cosStart =0;
		int cosEnd = 0;

		while(inputParam.contains("sqrt"))
		{
			StringBuilder newString = new StringBuilder();
			cosStart =0;
			cosEnd = 0;
			int count = 0;
			//Sets cosStart to point of first bracket
			cosStart = inputParam.indexOf("sqrt")+5;
			cosEnd =cosStart;
			//Runs once to get count to +1 and then exits when the count of brackets becomes zero again
			do {
				if(inputParam.charAt(cosEnd) =='(')
				{
					count++;
				}
				else if(inputParam.charAt(cosEnd)==')')
				{
					count--;
				}
				cosEnd++;
			}
			while (count !=0);

			//Changes
			String result = infixToPostfix(removeSqrt(inputParam.substring(cosStart, cosEnd),RadiansParam));
			result = calculatePostfix(result,RadiansParam);


			//Caclulate the actual cos value
			double cosValue = Double.parseDouble(result);
			cosValue = Math.sqrt(cosValue);
			result = Double.toString(cosValue);



			//Create new String
			newString.append(inputParam.substring(0, cosStart - 5));
			newString.append(result);
			newString.append(inputParam.substring(cosEnd, inputParam.length()));
			inputParam = newString.toString();
			//System.out.println("Sqrt root: " + inputParam);
		}
		//inputParam = removeUnary(inputParam);
		return inputParam;
	}

	static String removeCos(String inputParam,boolean RadiansParam) {
		int cosStart =0;
		int cosEnd = 0;

		while(inputParam.contains("cos"))
		{
			StringBuilder newString = new StringBuilder();
			cosStart =0;
			cosEnd = 0;
			int count = 0;
			//Sets cosStart to point of first bracker
			cosStart = inputParam.indexOf("cos")+4;
			cosEnd =cosStart;
			//Runs once to get count to +1 and then exits when the count of brackets becomes zero again
			do {
				if(inputParam.charAt(cosEnd) =='(')
				{
					count++;
				}
				else if(inputParam.charAt(cosEnd)==')')
				{
					count--;
				}
				cosEnd++;
			}
			while (count !=0);

			//Changes
			String result = infixToPostfix(removeCos(inputParam.substring(cosStart, cosEnd),RadiansParam));
			result = calculatePostfix(result,RadiansParam);


			//Calculate the actual cos value
			double cosValue = Double.parseDouble(result);
			if(RadiansParam){
				cosValue = Math.cos(Math.toRadians(cosValue));
			}
			else{
				cosValue = Math.cos(cosValue);
			}
			result = Double.toString(cosValue);



			//Create new String
			newString.append(inputParam.substring(0, cosStart - 4));
			newString.append(result);
			newString.append(inputParam.substring(cosEnd, inputParam.length()));
			inputParam = newString.toString();
			//System.out.println("New String: " + inputParam);
		}
		//inputParam = removeUnary(inputParam);
		return inputParam;
	}

	static String removeSin(String inputParam,boolean RadiansParam) {
		int cosStart =0;
		int cosEnd = 0;

		while(inputParam.contains("sin"))
		{
			StringBuilder newString = new StringBuilder();
			cosStart =0;
			cosEnd = 0;
			int count = 0;
			//Sets cosStart to point of first bracker
			cosStart = inputParam.indexOf("sin")+4;
			cosEnd =cosStart;
			//Runs once to get count to +1 and then exits when the count of brackets becomes zero again
			do {
				if(inputParam.charAt(cosEnd) =='(')
				{
					count++;
				}
				else if(inputParam.charAt(cosEnd)==')')
				{
					count--;
				}
				cosEnd++;
			}
			while (count !=0);

			//Changes
			String result = infixToPostfix(removeSin(inputParam.substring(cosStart, cosEnd),RadiansParam));
			result = calculatePostfix(result,RadiansParam);


			//Caclulate the actual cos value
			double cosValue = Double.parseDouble(result);
			if(RadiansParam){
				cosValue = Math.sin(cosValue * (Math.PI / 180));
			}
			else{
				cosValue = Math.sin(cosValue);
			}
			result = Double.toString(cosValue);



			//Create new String
			newString.append(inputParam.substring(0, cosStart - 4));
			newString.append(result);
			newString.append(inputParam.substring(cosEnd, inputParam.length()));
			inputParam = newString.toString();
			//System.out.println("New String: " + inputParam);
		}
		//inputParam = removeUnary(inputParam);
		return inputParam;
	}

	static String removeTan(String inputParam,boolean RadiansParam) {
		int cosStart =0;
		int cosEnd = 0;

		while(inputParam.contains("tan"))
		{
			StringBuilder newString = new StringBuilder();
			cosStart =0;
			cosEnd = 0;
			int count = 0;
			//Sets cosStart to point of first bracket
			cosStart = inputParam.indexOf("tan")+4;
			cosEnd =cosStart;
			//Runs once to get count to +1 and then exits when the count of brackets becomes zero again
			do {
				if(inputParam.charAt(cosEnd) =='(')
				{
					count++;
				}
				else if(inputParam.charAt(cosEnd)==')')
				{
					count--;
				}
				cosEnd++;
			}
			while (count !=0);

			//Changes
			String result = infixToPostfix(removeTan(inputParam.substring(cosStart, cosEnd),RadiansParam));
			result = calculatePostfix(result,RadiansParam);


			//Caclulate the actual tan value
			double cosValue = Double.parseDouble(result);
			if(RadiansParam){
				cosValue = Math.tan(cosValue* (Math.PI / 180));
			}
			else{
				cosValue = Math.tan(cosValue);
			}
			result = Double.toString(cosValue);



			//Create new String
			newString.append(inputParam.substring(0, cosStart - 4));
			newString.append(result);
			newString.append(inputParam.substring(cosEnd, inputParam.length()));
			inputParam = newString.toString();
			//System.out.println("New String: " + inputParam);
		}
		//inputParam = removeUnary(inputParam);
		return inputParam;
	}


//-----------------------------------------------------------------------------------------------------------------------------------------------------------
		static String removeLn(String inputParam,boolean RadiansParam) {
			int lnStart =0;
			int lnEnd = 0;

			while(inputParam.contains("ln"))
			{
				StringBuilder newString = new StringBuilder();
				lnStart =0;
				lnEnd = 0;
				int count = 0;
				//Sets lnStart to point of first bracket
				lnStart = inputParam.indexOf("ln")+3;
				lnEnd =lnStart;
				//Runs once to get count to +1 and then exits when the count of brackets becomes zero again
				do {
					if(inputParam.charAt(lnEnd) =='(')
					{
						count++;
					}
					else if(inputParam.charAt(lnEnd)==')')
					{
						count--;
					}
					lnEnd++;
				}
				while (count !=0);

				//Changes
				String result = infixToPostfix(removeLn(inputParam.substring(lnStart, lnEnd),RadiansParam));
				result = calculatePostfix(result,RadiansParam);


				//Caclulate the actual ln value
				double lnValue = Double.parseDouble(result);
				lnValue = Math.log(lnValue);
				result = Double.toString(lnValue);



				//Create new String
				newString.append(inputParam.substring(0, lnStart - 3));
				newString.append(result);
				newString.append(inputParam.substring(lnEnd, inputParam.length()));
				inputParam = newString.toString();
				//System.out.println("New String: " + inputParam);
			}
			//inputParam = removeUnary(inputParam);
			return inputParam;
		}

		static String removeExp(String inputParam,boolean RadiansParam) {
			int expStart =0;
			int expEnd = 0;

			while(inputParam.contains("exp"))
			{
				StringBuilder newString = new StringBuilder();
				expStart =0;
				expEnd = 0;
				int count = 0;
				//Sets expStart to point of first bracket
				expStart = inputParam.indexOf("exp")+4;
				expEnd =expStart;
				//Runs once to get count to +1 and then exits when the count of brackets becomes zero again
				do {
					if(inputParam.charAt(expEnd) =='(')
					{
						count++;
					}
					else if(inputParam.charAt(expEnd)==')')
					{
						count--;
					}
					expEnd++;
				}
				while (count !=0);

				//Changes
				String result = infixToPostfix(removeExp(inputParam.substring(expStart, expEnd),RadiansParam));
				result = calculatePostfix(result,RadiansParam);


				//Calculate the actual exp value
				double expValue = Double.parseDouble(result);
				expValue = Math.exp(expValue);
				result = Double.toString(expValue);



				//Create new String
				newString.append(inputParam.substring(0, expStart - 4));
				newString.append(result);
				newString.append(inputParam.substring(expEnd, inputParam.length()));
				inputParam = newString.toString();
				//System.out.println("New String: " + inputParam);
			}
			//inputParam = removeUnary(inputParam);
			return inputParam;
		}

		static String removeLog(String inputParam,boolean RadiansParam) {
			int logStart =0;
			int logEnd = 0;

			while(inputParam.contains("log"))
			{
				StringBuilder newString = new StringBuilder();
				logStart =0;
				logEnd = 0;
				int count = 0;
				//Sets lnStart to point of first bracket
				logStart = inputParam.indexOf("log")+4;
				logEnd =logStart;
				//Runs once to get count to +1 and then exits when the count of brackets becomes zero again
				do {
					if(inputParam.charAt(logEnd) =='(')
					{
						count++;
					}
					else if(inputParam.charAt(logEnd)==')')
					{
						count--;
					}
					logEnd++;
				}
				while (count !=0);

				//Changes
				String result = infixToPostfix(removeLog(inputParam.substring(logStart, logEnd),RadiansParam));
				result = calculatePostfix(result,RadiansParam);


				//Caclulate the actual log value
				double logValue = Double.parseDouble(result);
				logValue = Math.log10(logValue);
				result = Double.toString(logValue);



				//Create new String
				newString.append(inputParam.substring(0, logStart - 4));
				newString.append(result);
				newString.append(inputParam.substring(logEnd, inputParam.length()));
				inputParam = newString.toString();
				//System.out.println("New String: " + inputParam);
			}
			//inputParam = removeUnary(inputParam);
			return inputParam;
		}
//------------------------------------------------------------------------------------------------------------------------------------------------------------

	}

