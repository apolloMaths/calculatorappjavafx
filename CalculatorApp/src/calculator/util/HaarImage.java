package calculator.util;

public class HaarImage {

	public int size = 512;
	public double[][] PixelArray = new double[size][size];
	public double[][] tempArray;
	public double[] squareImage = new double[size * size];
	int[] CropedImage;
	int[] HaarOUT;
	int[][] HaarImage;
	int[][] OrigImg; int Width; int Height; int iter; double greyTemp = 0.0D;


	public HaarImage(int[] PArray, int h, int w, int iteration){
		iter = iteration;
		int step = 0;

		Width = w;
		Height = h;
		HaarImage = new int[Height][Width];

		for (int i = 0; i < Height; i++)
		{
			for (int j = 0; j < Width; j++)
			{
				greyTemp = PArray[step];
				PixelArray[i][j] = greyTemp;
				step++;
			}
		}
		tempArray = new double[size][size];
		HaarOUT = new int[size * size];
		decompImage();
	}

	public void decompImage() {
		int index = size;
	    int i = 0;
	    while ((i < iter) && (index >= 2))
	    {
	      int offset = index / 2;

	      for (int k = 0; k < size; k++)
	      {
	        for (int j = 0; j < index; j += 2)
	        {
	          double avg = (PixelArray[k][j] + PixelArray[k][(j + 1)]) / 2.0D;
	          double diff = PixelArray[k][j] - avg;

	          tempArray[k][(j / 2)] = avg;
	          tempArray[k][(offset + j / 2)] = diff;
	        }
	      }

	      for (int k = 0; k < size; k++) for (int j = 0; j < index; j++) { PixelArray[k][j] = tempArray[k][j];
	        }
	      for (int j = 0; j < size; j++)
	      {
	        for (int k = 0; k < index; k += 2)
	        {
	          double avg = (tempArray[k][j] + tempArray[(k + 1)][j]) / 2.0D;
	          double diff = tempArray[k][j] - avg;

	          PixelArray[(k / 2)][j] = avg;
	          PixelArray[(offset + k / 2)][j] = diff;
	        }
	      }

	      for (int j = 0; j < size; j++) for (int k = 0; k < index; k++) tempArray[k][j] = PixelArray[k][j];
	      index /= 2;
	      i++;
	    }
	}
}

