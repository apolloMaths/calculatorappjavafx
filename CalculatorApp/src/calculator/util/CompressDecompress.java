package calculator.util;

import java.text.DecimalFormat;

public class CompressDecompress {

	static int N = 8;
	static double n = 8.0;
	static double[][] T = new double[N][N];
	static double[][] Tt = new double[N][N];
	static double[][] M ={{16,8,23,16,5,14,7,22},
			{20,14,22,7,14,22,24,6},
			{15,23,24,23,9,6,6,20},
			{14,8,11,14,12,12,25,10},
			{10,9,11,9,13,19,5,17},
			{8,22,20,15,12,8,22,17},
			{24,22,17,12,18,11,23,14},
			{21,25,15,16,23,14,22,22}
	};
	static double[][] TM = new double[N][N];
	static double[][] DCT = new double[N][N];
	static double[][] R = new double[N][N];
	static double[][] RTt = new double[N][N];
	static double[][] IDCT = new double[N][N];
	static int[][] quantization = new int[N][N];
	static int[][] q50 ={{16,11,10,16,24,40,51,61},
			{12,12,14,19,26,58,60,55},
			{14,13,16,24,40,57,69,56},
			{14,17,22,29,51,87,80,62},
			{18,22,37,56,68,109,103,77},
			{24,35,55,64,81,104,113,92},
			{49,64,78,87,103,121,120,101},
			{72,92,95,98,112,100,103,99}
	};
	static double[][] C = new double[N][N];
	static String qlevel;
	static DecimalFormat df = new DecimalFormat("####0.000");

	public CompressDecompress(String level) {
		qlevel = level;
	}
	public void compress() {

		generate_T();//generates both T and Tt
		getQuantizationBlock(Integer.parseInt(qlevel));
		TM = multiplyMat(T, M);
		DCT = multiplyMat(TM, Tt);
		C = quantizationOfDCT(DCT, quantization);		
	}
	public static  String getQ() {
		String Q = "";
		for (int r = 0; r < N; r++) {
			for (int c = 0; c < N; c++) {
				Q += Math.round(quantization[r][c]) + " ";
			}
			Q += "\n";
		}
		return Q;
	}
	public static  String getMessage() {
		String Message = "";
		for (int r = 0; r < N; r++) {
			for (int c = 0; c < N; c++) {
				Message +=  Math.round(M[r][c]) + " ";
			}
			Message += "\n";
		}
		return Message;
	} 

	public static  String getCompressed() {
		String Comp = "";
		for (int r = 0; r < N; r++) {
			for (int c = 0; c < N; c++) {
				Comp +=  Math.round(C[r][c]) + " ";
			}
			Comp += "\n";
		}
		return Comp;
	}
	 void generate_T(){
		for (int R = 0; R < N; R++) {
			for (int C = 0; C < N; C++) {
				if(R==0){
					T[R][C] = 1/Math.sqrt(n);
					Tt[C][R] = T[R][C] ;
				}else{
					T[R][C] = Math.sqrt((2.0/n))*Math.cos((R*Math.PI*(2*C+1))/(2*n));
					Tt[C][R] = T[R][C] ;
				}
			}
		}
	}

	 double[][] quantizationOfDCT(double[][] DCT,int[][] QB){
		double[][] result = new double[N][N];
		for (int R = 0; R < N; R++) {
			for (int C = 0; C < N; C++) {
				result[R][C] = Math.round(DCT[R][C] / QB[R][C]);
			}
		}
		return result;
	}

	 double[][] multiplyMat(double[][] matrixA, double[][] matrixB){
		double[][] result = new double[N][N];
		double sum = 0.0;
		for (int R = 0; R < N; R++) {
			for (int C = 0; C < N; C++) {
				sum = 0.0;
				for (int K = 0; K < N; K++) {
					sum += matrixA[R][K] * matrixB[K][C];
				}
				result[R][C] = sum;
			}
		}
		return result;
	}


	 void getQuantizationBlock(int qLevel){	

		if(qLevel == 50){
			quantization = q50;
		}
		else{
			for (int r = 0; r <n ; r++) {
				for (int c = 0; c < n ; c++) {
					if(qLevel>50){
						quantization[r][c] = (q50[r][c]*(100-qLevel)/50);
					}else{
						quantization[r][c] = (q50[r][c]*(50/qLevel));
					}
				}
			}
		}
	}

	public void decompression() {
		double[][] result = new double[N][N];
		for (int R = 0; R < N; R++) {
			for (int c = 0; c < N; c++) {
				result[R][c] = C[R][c] * quantization[R][c];
			}
		}

		R = result;		
		RTt = multiplyMat(Tt,R);
		IDCT = multiplyMat(RTt, T);
	}
	
	public static  String getDecompressed(String text) {
		String deCompressed = "";
		for (int r = 0; r < N; r++) {
			for (int c = 0; c < N; c++) {
				deCompressed +=  Math.round(IDCT[r][c]) + " ";
			}
			deCompressed += "\n";
		}
		return deCompressed;
	}

}
