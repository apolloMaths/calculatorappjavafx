package calculator.view;

import javax.swing.JPanel;
import javax.swing.event.ChangeListener;

import calculator.exceptions.WrongRangeException;
import calculator.view.graphics.DrawAllEquation;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingNode;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.AnchorPane;

public class AllEqGraphController {

	@FXML
	private TextField textField_parA;
	@FXML
	private TextField textField_parB;
	@FXML
	private TextField textField_parC;
	@FXML
	private TextField textField_startRange;
	@FXML
	private TextField textField_EndRange;
	@FXML
	private AnchorPane drawAnchorPane;

	@FXML
	private TextField textField_equation;

	@FXML
	private RadioButton cubicEqRB;

	@FXML
	private RadioButton sinEqRB;

	@FXML
	private RadioButton cosEqRB;

	@FXML
	private RadioButton tangEqRB;

	@FXML
	private RadioButton ctangEqRB;

	@FXML
	private RadioButton lnEqRB;

	@FXML
	private RadioButton oneOverAEqRB;

	@FXML
	private ToggleGroup equationTG;

	@FXML
	private void drawButtonHandle() {

		String equation = "";
		try {
			equation = textField_equation.getText();
		} catch (Exception e) {
			equation = "";
		}
		System.out.println("selected " + cubicEqRB.isSelected());

		if (equation.equals("")) {
			if (cubicEqRB.isSelected()) {
				equation = "cubic";
			} else if (sinEqRB.isSelected()) {
				equation = "sin";
			} else if (cosEqRB.isSelected()) {
				equation = "cos";
			} else if (tangEqRB.isSelected()) {
				equation = "tan";
			} else if (lnEqRB.isSelected()) {
				equation = "ln";
			} else if (oneOverAEqRB.isSelected()) {
				equation = "oneover";
			}
		}
		System.out.println("equation: " + equation);
		SwingNode swingNode = new SwingNode();
		double xRangeStart = -4;
		double xRangeEnd = 4;
		double a = 1;
		double b = 1;
		double c = 1;
		try {
			xRangeStart = Double.parseDouble(textField_startRange.getText());
			xRangeEnd = Double.parseDouble(textField_EndRange.getText());
			a = Double.parseDouble(textField_parA.getText());
			b = Double.parseDouble(textField_parB.getText());
			c = Double.parseDouble(textField_parC.getText());
			if (xRangeStart >= xRangeEnd) {
				throw new WrongRangeException("x start range must be lower then x end range");
			}
		} catch (Exception e1) {
			Alert alert = new Alert(AlertType.ERROR, e1.getMessage() + "\n Default values will be used",
					ButtonType.CLOSE);
			alert.showAndWait();

			xRangeStart = -4;
			xRangeEnd = 4;
			a = 1;
			b = 1;
			c = 1;
			textField_startRange.setText("-4");
			textField_EndRange.setText("4");
			textField_parA.setText("1");
			textField_parB.setText("1");
			textField_parC.setText("1");
		}

		System.out.println("QuadraticEqGraphController.drawButtonHandle()");
		JPanel graph = new DrawAllEquation(xRangeStart, xRangeEnd, a, b, c, equation);
		swingNode.setContent(graph);
		drawAnchorPane.getChildren().add(swingNode);
	}

	@FXML
	public void start() {
		System.out.println("second");
		cubicEqRB = new RadioButton();
		cubicEqRB.setUserData("CUBIC");
		sinEqRB = new RadioButton();
		sinEqRB.setSelected(true);
		cosEqRB = new RadioButton();
		tangEqRB = new RadioButton();
		ctangEqRB = new RadioButton();
		lnEqRB = new RadioButton();
		oneOverAEqRB = new RadioButton();
		equationTG = new ToggleGroup();

		cubicEqRB.setToggleGroup(equationTG);
		sinEqRB.setToggleGroup(equationTG);
		cosEqRB.setToggleGroup(equationTG);
		tangEqRB.setToggleGroup(equationTG);
		ctangEqRB.setToggleGroup(equationTG);
		lnEqRB.setToggleGroup(equationTG);
		oneOverAEqRB.setToggleGroup(equationTG);
	}

	public TextField getTextField_parA() {
		return textField_parA;
	}

	public void setTextField_parA(TextField textField_parA) {
		this.textField_parA = textField_parA;
	}

	public TextField getTextField_parB() {
		return textField_parB;
	}

	public void setTextField_parB(TextField textField_parB) {
		this.textField_parB = textField_parB;
	}

	public TextField getTextField_parC() {
		return textField_parC;
	}

	public void setTextField_parC(TextField textField_parC) {
		this.textField_parC = textField_parC;
	}

	public TextField getTextField_startRange() {
		return textField_startRange;
	}

	public void setTextField_startRange(TextField textField_startRange) {
		this.textField_startRange = textField_startRange;
	}

	public TextField getTextField_EndRange() {
		return textField_EndRange;
	}

	public void setTextField_EndRange(TextField textField_EndRange) {
		this.textField_EndRange = textField_EndRange;
	}

	public AnchorPane getDrawAnchorPane() {
		return drawAnchorPane;
	}

	public void setDrawAnchorPane(AnchorPane drawAnchorPane) {
		this.drawAnchorPane = drawAnchorPane;
	}

	public RadioButton getCubicEqRB() {
		return cubicEqRB;
	}

	public void setCubicEqRB(RadioButton cubicEqRB) {
		this.cubicEqRB = cubicEqRB;
	}

	public ToggleGroup getEquationTG() {
		return equationTG;
	}

	public void setEquationTG(ToggleGroup equationTG) {
		this.equationTG = equationTG;
	}

}
