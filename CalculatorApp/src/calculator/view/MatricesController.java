package calculator.view;

import calculator.MainApp;

import calculator.util.Matrices;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class MatricesController {

	private MainApp mainApp;

	Matrices matrices = new Matrices();
	@FXML
    private TextArea resultArea;
	@FXML
    private TextField multByTextField;
	@FXML
    private TextField divByTextField;
	@FXML
    private TextArea matrixAtextArea;
	@FXML
    private TextArea matrixBtextArea;
	@FXML
    private TextField numRowsA;
	@FXML
    private TextField numColumnsA;
	@FXML
    private TextField numRowsB;
	@FXML
    private TextField numColumnsB;
	

	@FXML
    private void handleAplusB() {
	
		resultArea.setText(Matrices.Display(Matrices.Add(Matrices.ReadInMatrices(matrixAtextArea.getText(),Integer.valueOf(numRowsA.getText()), Integer.valueOf(numColumnsA.getText())), Matrices.ReadInMatrices(matrixBtextArea.getText(),Integer.valueOf(numRowsB.getText()),Integer.valueOf(numColumnsB.getText())), Integer.valueOf(numRowsA.getText()), Integer.valueOf(numColumnsA.getText()), Integer.valueOf(numRowsB.getText()),Integer.valueOf(numColumnsB.getText())), Integer.valueOf(numRowsA.getText()), Integer.valueOf(numColumnsA.getText())));
	}
	
	@FXML
    private void handleAminusB() {
	
		resultArea.setText(Matrices.Display(Matrices.AsubtractB(Matrices.ReadInMatrices(matrixAtextArea.getText(),Integer.valueOf(numRowsA.getText()), Integer.valueOf(numColumnsA.getText())), Matrices.ReadInMatrices(matrixBtextArea.getText(),Integer.valueOf(numRowsB.getText()),Integer.valueOf(numColumnsB.getText())), Integer.valueOf(numRowsA.getText()), Integer.valueOf(numColumnsA.getText()), Integer.valueOf(numRowsB.getText()),Integer.valueOf(numColumnsB.getText())), Integer.valueOf(numRowsA.getText()), Integer.valueOf(numColumnsA.getText())));
    }
	
	@FXML
    private void handleBminusA() {
	
		resultArea.setText(Matrices.Display(Matrices.BsubtractA(Matrices.ReadInMatrices(matrixAtextArea.getText(),Integer.valueOf(numRowsA.getText()), Integer.valueOf(numColumnsA.getText())), Matrices.ReadInMatrices(matrixBtextArea.getText(),Integer.valueOf(numRowsB.getText()),Integer.valueOf(numColumnsB.getText())), Integer.valueOf(numRowsA.getText()), Integer.valueOf(numColumnsA.getText()), Integer.valueOf(numRowsB.getText()),Integer.valueOf(numColumnsB.getText())), Integer.valueOf(numRowsA.getText()), Integer.valueOf(numColumnsA.getText())));
    }
	
	@FXML
    private void handleAmultB() {
	
		resultArea.setText(Matrices.DisplayMult(Matrices.Multiply(Matrices.ReadInMatrices(matrixAtextArea.getText(),Integer.valueOf(numRowsA.getText()), Integer.valueOf(numColumnsA.getText())), Matrices.ReadInMatrices(matrixBtextArea.getText(),Integer.valueOf(numRowsB.getText()),Integer.valueOf(numColumnsB.getText())), Integer.valueOf(numRowsA.getText()), Integer.valueOf(numColumnsA.getText()), Integer.valueOf(numRowsB.getText()),Integer.valueOf(numColumnsB.getText())), Integer.valueOf(numRowsA.getText()), Integer.valueOf(numColumnsB.getText())));
    }
	
	@FXML
    private void handleBmultA() {
	
		resultArea.setText(Matrices.DisplayMult(Matrices.Multiply(Matrices.ReadInMatrices(matrixBtextArea.getText(),Integer.valueOf(numRowsB.getText()), Integer.valueOf(numColumnsB.getText())), Matrices.ReadInMatrices(matrixAtextArea.getText(),Integer.valueOf(numRowsA.getText()),Integer.valueOf(numColumnsA.getText())), Integer.valueOf(numRowsB.getText()), Integer.valueOf(numColumnsB.getText()), Integer.valueOf(numRowsA.getText()),Integer.valueOf(numColumnsA.getText())), Integer.valueOf(numRowsB.getText()), Integer.valueOf(numColumnsA.getText())));
    }
	
	@FXML
    private void handleTransposeA() {
	
		resultArea.setText(Matrices.DisplayTranspose(Matrices.Transpose(Matrices.ReadInMatrices(matrixAtextArea.getText(), Integer.valueOf(numRowsA.getText()), Integer.valueOf(numColumnsA.getText())), Integer.valueOf(numRowsA.getText()), Integer.valueOf(numColumnsA.getText())), Integer.valueOf(numRowsA.getText()), Integer.valueOf(numColumnsA.getText())));
    }
	
	@FXML
    private void handleTransposeB() {
	
		resultArea.setText(Matrices.DisplayTranspose(Matrices.Transpose(Matrices.ReadInMatrices(matrixBtextArea.getText(), Integer.valueOf(numRowsB.getText()), Integer.valueOf(numColumnsB.getText())), Integer.valueOf(numRowsB.getText()), Integer.valueOf(numColumnsB.getText())), Integer.valueOf(numRowsB.getText()), Integer.valueOf(numColumnsB.getText())));
    }
	
	@FXML
    private void handleDeterminantA() {
	
		resultArea.setText(String.valueOf(Matrices.Determinant(Matrices.ReadInMatrices(matrixAtextArea.getText(), Integer.valueOf(numRowsA.getText()), Integer.valueOf(numColumnsA.getText())), Integer.valueOf(numRowsA.getText()), Integer.valueOf(numColumnsA.getText()))));
    }
	
	@FXML
    private void handleDeterminantB() {
	
		resultArea.setText(String.valueOf(Matrices.Determinant(Matrices.ReadInMatrices(matrixBtextArea.getText(), Integer.valueOf(numRowsB.getText()), Integer.valueOf(numColumnsB.getText())), Integer.valueOf(numRowsB.getText()), Integer.valueOf(numColumnsB.getText()))));
    }
	
	@FXML
    private void handleMultiplyMatrixAbyX() {
	
		resultArea.setText(Matrices.Display(Matrices.MultiplyMatrixbyX(Matrices.ReadInMatrices(matrixAtextArea.getText(),Integer.valueOf(numRowsA.getText()),Integer.valueOf(numColumnsA.getText())), Double.valueOf(multByTextField.getText()),Integer.valueOf(numRowsA.getText()), Integer.valueOf(numColumnsA.getText())), Integer.valueOf(numRowsA.getText()), Integer.valueOf(numColumnsA.getText())));
    }
	
	@FXML
    private void handleMultiplyMatrixBbyX() {
	
		resultArea.setText(Matrices.Display(Matrices.MultiplyMatrixbyX(Matrices.ReadInMatrices(matrixBtextArea.getText(),Integer.valueOf(numRowsB.getText()),Integer.valueOf(numColumnsB.getText())), Double.valueOf(multByTextField.getText()),Integer.valueOf(numRowsB.getText()), Integer.valueOf(numColumnsB.getText())), Integer.valueOf(numRowsB.getText()), Integer.valueOf(numColumnsB.getText())));
    }
	
	@FXML
    private void handleDivideMatrixA() {
	
		resultArea.setText(Matrices.Display(Matrices.DivideA(Matrices.ReadInMatrices(matrixAtextArea.getText(),Integer.valueOf(numRowsA.getText()), Integer.valueOf(numColumnsA.getText())), Matrices.ReadInMatrices(matrixBtextArea.getText(),Integer.valueOf(numRowsB.getText()),Integer.valueOf(numColumnsB.getText())), Integer.valueOf(numRowsA.getText()), Integer.valueOf(numColumnsA.getText()), Integer.valueOf(numRowsB.getText()),Integer.valueOf(numColumnsB.getText())), Integer.valueOf(numRowsA.getText()), Integer.valueOf(numColumnsA.getText())));
    }
		
	@FXML
    private void handleInverseA() {
	
		resultArea.setText(Matrices.Display(Matrices.Inverse(Matrices.ReadInMatrices(matrixAtextArea.getText(),Integer.valueOf(numRowsA.getText()),Integer.valueOf(numColumnsA.getText())),Integer.valueOf(numRowsA.getText()),Integer.valueOf(numColumnsA.getText())),Integer.valueOf(numRowsA.getText()), Integer.valueOf(numColumnsA.getText())));
    }
	
	@FXML
    private void handleInverseB() {
	
		resultArea.setText(Matrices.Display(Matrices.Inverse(Matrices.ReadInMatrices(matrixBtextArea.getText(),Integer.valueOf(numRowsB.getText()),Integer.valueOf(numColumnsB.getText())),Integer.valueOf(numRowsB.getText()),Integer.valueOf(numColumnsB.getText())),Integer.valueOf(numRowsB.getText()), Integer.valueOf(numColumnsB.getText())));
    }
	
	@FXML
    private void handleAdivX() {
	
		resultArea.setText(Matrices.Display(Matrices.DivideMatrixbyX(Matrices.ReadInMatrices(matrixAtextArea.getText(),Integer.valueOf(numRowsA.getText()),Integer.valueOf(numColumnsA.getText())), Double.valueOf(divByTextField.getText()),Integer.valueOf(numRowsA.getText()), Integer.valueOf(numColumnsA.getText())), Integer.valueOf(numRowsA.getText()), Integer.valueOf(numColumnsA.getText())));
    }
	
	@FXML
    private void handleBdivX() {
	
		resultArea.setText(Matrices.Display(Matrices.DivideMatrixbyX(Matrices.ReadInMatrices(matrixBtextArea.getText(),Integer.valueOf(numRowsB.getText()),Integer.valueOf(numColumnsB.getText())), Double.valueOf(divByTextField.getText()),Integer.valueOf(numRowsB.getText()), Integer.valueOf(numColumnsB.getText())), Integer.valueOf(numRowsB.getText()), Integer.valueOf(numColumnsB.getText())));
    }
	
	@FXML
    private void handleAdjointA() {
	
		resultArea.setText(Matrices.Display(Matrices.Adjoint(Matrices.ReadInMatrices(matrixAtextArea.getText(),Integer.valueOf(numRowsA.getText()),Integer.valueOf(numColumnsA.getText())),Integer.valueOf(numRowsA.getText()),Integer.valueOf(numColumnsA.getText())),Integer.valueOf(numRowsA.getText()), Integer.valueOf(numColumnsA.getText())));
    }
	
	@FXML
    private void handleAdjointB() {
	
		resultArea.setText(Matrices.Display(Matrices.Adjoint(Matrices.ReadInMatrices(matrixBtextArea.getText(),Integer.valueOf(numRowsB.getText()),Integer.valueOf(numColumnsB.getText())),Integer.valueOf(numRowsB.getText()),Integer.valueOf(numColumnsB.getText())),Integer.valueOf(numRowsB.getText()), Integer.valueOf(numColumnsB.getText())));
    }
	
	
	
	
	
	
	
}
