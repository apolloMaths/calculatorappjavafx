package calculator.view;

import javax.swing.JPanel;

import calculator.exceptions.WrongRangeException;
import calculator.view.graphics.DrawLinearEquation;
import calculator.view.graphics.DrawQuadraticEquation;
import javafx.embed.swing.SwingNode;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

public class LinearEqGraphController {

	@FXML
	private TextField textField_parA;
	@FXML
	private TextField textField_parB;

	@FXML
	private TextField textField_startRange;
	@FXML
	private TextField textField_EndRange;
	@FXML
	private AnchorPane drawAnchorPane;

	@FXML
	private void drawButtonHandle() {
		System.out.println("QuadraticEqGraphController.drawButtonHandle()");
		SwingNode swingNode = new SwingNode();

		double xRangeStart = -4;
		double xRangeEnd = 4;
		double a = 1;
		double b = 1;
		try {
			xRangeStart = Double.parseDouble(textField_startRange.getText());
			xRangeEnd = Double.parseDouble(textField_EndRange.getText());
			a = Double.parseDouble(textField_parA.getText());
			b = Double.parseDouble(textField_parB.getText());

			if (xRangeStart >= xRangeEnd) {
				throw new WrongRangeException("x start range must be lower then x end range");
			}
		} catch (Exception e1) {
			Alert alert = new Alert(AlertType.ERROR, e1.getMessage() + "\n Default values will be used", ButtonType.CLOSE);
			alert.showAndWait();

			xRangeStart = -4;
			xRangeEnd = 4;
			a = 1;
			b = 1;

			textField_startRange.setText("-4");
			textField_EndRange.setText("4");
			textField_parA.setText("1");
			textField_parB.setText("1");

		}

		JPanel graph = new DrawLinearEquation(xRangeStart, xRangeEnd, a, b);
		swingNode.setContent(graph);
		drawAnchorPane.getChildren().add(swingNode);
	}

	public TextField getTextField_parA() {
		return textField_parA;
	}

	public void setTextField_parA(TextField textField_parA) {
		this.textField_parA = textField_parA;
	}

	public TextField getTextField_parB() {
		return textField_parB;
	}

	public void setTextField_parB(TextField textField_parB) {
		this.textField_parB = textField_parB;
	}

//	public TextField getTextField_parC() {
//		return textField_parC;
//	}
//
//	public void setTextField_parC(TextField textField_parC) {
//		this.textField_parC = textField_parC;
//	}

	public TextField getTextField_startRange() {
		return textField_startRange;
	}

	public void setTextField_startRange(TextField textField_startRange) {
		this.textField_startRange = textField_startRange;
	}

	public TextField getTextField_EndRange() {
		return textField_EndRange;
	}

	public void setTextField_EndRange(TextField textField_EndRange) {
		this.textField_EndRange = textField_EndRange;
	}

	public AnchorPane getDrawAnchorPane() {
		return drawAnchorPane;
	}

	public void setDrawAnchorPane(AnchorPane drawAnchorPane) {
		this.drawAnchorPane = drawAnchorPane;
	}

}
