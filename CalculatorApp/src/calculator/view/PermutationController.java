package calculator.view;

import java.math.BigInteger;

import calculator.MainApp;
import calculator.model.Equation;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import calculator.util.*;


public class PermutationController {

	private MainApp mainApp;
	Permutations perm = new Permutations();
//	ObservableList<String> iterations=FXCollections.observableArrayList("1", "2", "3");
	private ObservableList<Equation> permutationData = FXCollections.observableArrayList();

	@FXML
	private TableView<Equation> equationTable;

	@FXML
	private TableColumn<Equation,String> equationColumn;

	@FXML
    private TextField numberSetField;

	@FXML
    private TextField resultField;

	@FXML
    private TextField numberOfObjectsField;

	@FXML
    private TextField stringSetField;

	@FXML
    private TextField resultFieldString;

	@FXML
    private TextField numberOfObjectsFieldString;

	@FXML
    private CheckBox orderCheckBox;

	@FXML
    private CheckBox repeatedCheckBox;
	
	@FXML
    private ComboBox stringComboBox;
	
	 @FXML
	    private void stringIterations() {
		 
//		    	int iterations =Integer.parseInt((String) stringComboBox.getValue());
//		    	System.out.println("Iterations: " + iterations);
//		    	switch(iterations) {
//				case 1:
//					stringSetField.setText("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
//					break;
//				case 2:
//					stringSetField.setText("abcdefghijklmnopqrstuvwxyz");
//					break;
//				case 3:
//					stringSetField.setText("0123456789");
//				}
		    	
		    	int iterations =Integer.parseInt((String) stringComboBox.getValue());
		    	System.out.println("Iterations: " + iterations);
		    	for (int i=0;i<iterations;i++){
		    		
		    	}
		    	
//		    	if(iterations==1) {
//		    		stringSetField.setText("abcdefghijklmnopqrstuvwxyz");
//		    		else if () {
//		    			
//		    		}
		    		
		    		
//		    	}
	 }

	@FXML
	private void handleNumberCalculate() {
		//		System.out.println("handleNumberCalulate()");
		//		System.out.println("order:" + orderCheckBox.isSelected());
		//		System.out.println("order:" + repeatedCheckBox.isSelected());
		if(Integer.parseInt(numberSetField.getText()) < Integer.parseInt(numberOfObjectsField.getText())) {
			resultField.setText("Set size is too small for objects selected");
		} else {
			if(orderCheckBox.isSelected() & repeatedCheckBox.isSelected()) {
				resultField.setText("Permutation with repititon: " + Permutations.toStringNum(Permutations.number(Integer.parseInt(numberSetField.getText()), Integer.parseInt(numberOfObjectsField.getText()), repeatedCheckBox.isSelected(), orderCheckBox.isSelected())));
			} else if (!orderCheckBox.isSelected() & !repeatedCheckBox.isSelected()){
				resultField.setText("Combination without repitiion: " + Permutations.toStringNum(Permutations.number(Integer.parseInt(numberSetField.getText()), Integer.parseInt(numberOfObjectsField.getText()), repeatedCheckBox.isSelected(), false)));
			} else if (!orderCheckBox.isSelected() & repeatedCheckBox.isSelected()) {
				resultField.setText("Combination with repitition: " + Permutations.toStringNum(Permutations.number(Integer.parseInt(numberSetField.getText()),Integer.parseInt(numberOfObjectsField.getText()),repeatedCheckBox.isSelected(),orderCheckBox.isSelected())));
			} else if (orderCheckBox.isSelected() & !repeatedCheckBox.isSelected()) {
				resultField.setText("Permutation without repititon: " + Permutations.toStringNum(Permutations.number(Integer.parseInt(numberSetField.getText()),Integer.parseInt(numberOfObjectsField.getText()),repeatedCheckBox.isSelected(),orderCheckBox.isSelected())));
			}
		}
	}



	@FXML
    private void handleStringCalculate() {

		resultFieldString.setText(""+Permutations.possibleStrings(Integer.parseInt(numberOfObjectsFieldString.getText()),stringSetField.getText().toCharArray(), ""));
    }

}
