package calculator.view;

import calculator.util.CompressDecompress;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class DCTCompressionDecompressionControl {
	private CompressDecompress compressDecompress;
	
	@FXML
    private TextField quantizationLevelText;
	
	@FXML
    private TextArea quantizationBlockArea;
	
	@FXML
    private TextArea toBeCompressedBlockArea;
	
	@FXML
    private TextArea compressedBlockArea;
	
	@FXML
    private TextArea decompressedBlockArea;
	
	
	@FXML
    private void handleCompression() {
		compressDecompress = new CompressDecompress(quantizationLevelText.getText());
		compressDecompress.compress();
		quantizationBlockArea.setText(CompressDecompress.getQ());
		toBeCompressedBlockArea.setText(CompressDecompress.getMessage());
		compressedBlockArea.setText(CompressDecompress.getCompressed());
    }
	
	@FXML
	private void handleDecompression() {
		compressDecompress = new CompressDecompress(quantizationLevelText.getText());
		compressDecompress.decompression();
		decompressedBlockArea.setText(CompressDecompress.getDecompressed(decompressedBlockArea.getText()));
	}

}
