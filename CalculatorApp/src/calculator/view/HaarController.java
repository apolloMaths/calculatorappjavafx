package calculator.view;

import calculator.MainApp;
import calculator.model.Equation;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import calculator.util.*;
public class HaarController {

	// Reference to the main application.
    private MainApp mainApp;
    HaarDecompositionRecomposition haar = new HaarDecompositionRecomposition();
    
    ObservableList<String> iterations=FXCollections.observableArrayList("1","2","3");

    @FXML
    private TextField t00;

    @FXML
    private TextField t01;

    @FXML
    private TextField t02;

    @FXML
    private TextField t03;

    @FXML
    private TextField t04;

    @FXML
    private TextField t05;

    @FXML
    private TextField t06;

    @FXML
    private TextField t07;

    @FXML
    private TextField t10;

    @FXML
    private TextField t11;

    @FXML
    private TextField t12;

    @FXML
    private TextField t13;

    @FXML
    private TextField t14;

    @FXML
    private TextField t15;

    @FXML
    private TextField t16;

    @FXML
    private TextField t17;

    @FXML
    private TextField t20;

    @FXML
    private TextField t21;

    @FXML
    private TextField t22;

    @FXML
    private TextField t23;

    @FXML
    private TextField t24;

    @FXML
    private TextField t25;

    @FXML
    private TextField t26;

    @FXML
    private TextField t27;

    @FXML
    private TextField t30;

    @FXML
    private TextField t31;

    @FXML
    private TextField t32;

    @FXML
    private TextField t33;

    @FXML
    private TextField t34;

    @FXML
    private TextField t35;

    @FXML
    private TextField t36;

    @FXML
    private TextField t37;

    @FXML
    private TextField t40;

    @FXML
    private TextField t41;

    @FXML
    private TextField t42;

    @FXML
    private TextField t43;

    @FXML
    private TextField t44;

    @FXML
    private TextField t45;

    @FXML
    private TextField t46;

    @FXML
    private TextField t47;

    @FXML
    private TextField t50;

    @FXML
    private TextField t51;

    @FXML
    private TextField t52;

    @FXML
    private TextField t53;

    @FXML
    private TextField t54;

    @FXML
    private TextField t55;

    @FXML
    private TextField t56;

    @FXML
    private TextField t57;

    @FXML
    private TextField t60;

    @FXML
    private TextField t61;

    @FXML
    private TextField t62;

    @FXML
    private TextField t63;

    @FXML
    private TextField t64;

    @FXML
    private TextField t65;

    @FXML
    private TextField t66;

    @FXML
    private TextField t67;

    @FXML
    private TextField t70;

    @FXML
    private TextField t71;

    @FXML
    private TextField t72;

    @FXML
    private TextField t73;

    @FXML
    private TextField t74;

    @FXML
    private TextField t75;

    @FXML
    private TextField t76;

    @FXML
    private TextField t77;
    
    @FXML 
    private ComboBox iterationBox;

    private TextField[] arrayOfTextfields;

    @FXML
    private void handleDecompose() {
    	double [][] arrayParam= new double[8][8];

    	int row1 =0;
    	int column1=0;
    	for(TextField text:arrayOfTextfields){
    		arrayParam[row1][column1] = Double.parseDouble(text.getText());
    		column1++;
    		if(column1 == 8 ){
    			row1++;
    			column1=0;
    		}
    	}
    	try{
    	int iterations =Integer.parseInt((String) iterationBox.getValue());
    	System.out.println("Iterations: " + iterations);
    	for (int i=0;i<iterations;i++){
    		arrayParam =haar.decompose(arrayParam);
    	}
    	int row2 =0;
    	int column2 = 0;
    	//arrayOfTextfields[2].setText("Work");
    	for(TextField text :arrayOfTextfields){
    		text.setText(Integer.toString(((int)arrayParam[row2][column2])));
    		System.out.println("Row:" + row2);
    		column2++;
    		if(column2 == 8 ){
    			row2++;
    			column2 =0;
    		}
    	}
    	}catch (Exception e1) {
			Alert alert = new Alert(AlertType.ERROR,"Select Number of Iterations !!", ButtonType.CLOSE);
			alert.showAndWait();
    	}

    }

    @FXML
    private void handleRecomposition(){
    	double [][] arrayParam= new double[8][8];

    	int row1 =0;
    	int column1=0;
    	for(TextField text:arrayOfTextfields){
    		arrayParam[row1][column1] = Double.parseDouble(text.getText());
    		column1++;
    		if(column1 == 8 ){
    			row1++;
    			column1=0;
    		}
    	}
    	
    	arrayParam=haar.recompose(arrayParam);
    	int row =0;
    	int column = 0;
    	for(TextField text :arrayOfTextfields){
    		text.setText(Integer.toString(((int)arrayParam[row][column])));
    		System.out.println("Row:" + row);
    		column++;
    		if(column == 8 ){
    			row++;
    			column =0;
    		}
    	}
    	
    }

	public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }

	public void initialize(){
		arrayOfTextfields = new TextField[]{t00,t01,t02,t03,t04,t05,t06,t07,
							t10,t11,t12,t13,t14,t15,t16,t17,
							t20,t21,t22,t23,t24,t25,t26,t27,
							t30,t31,t32,t33,t34,t35,t36,t37,
							t40,t41,t42,t43,t44,t45,t46,t47,
							t50,t51,t52,t53,t54,t55,t56,t57,
							t60,t61,t62,t63,t64,t65,t66,t67,
							t70,t71,t72,t73,t74,t75,t76,t77};
		
		iterationBox.setItems(iterations);
		
	}
}
