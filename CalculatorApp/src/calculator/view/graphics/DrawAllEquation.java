package calculator.view.graphics;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.AffineTransform;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.swing.JPanel;

import calculator.util.ShuntingYardAlgorithm;

public class DrawAllEquation extends JPanel {

	private static final long serialVersionUID = 4686138979547533752L;
	private String equation = "(2*x^3)";
	private boolean isPeriodic = true;
	DecimalFormat dc = new DecimalFormat("#.##");
	private double xRangeEnd, xRangeStart, yRangeTop = 0, yRangeBottom = 0, parA, parB, parC, metric;
	ArrayList<Point> listOfPoints;
	private double metricX = 0.0;
	private double metricY = 0.0;

	public DrawAllEquation(double xRangeStart, double xRangeEnd, double parA, double parB, double parC,
			String equation) {
		setPreferredSize(getPreferredSize());
		this.xRangeStart = xRangeStart;
		this.xRangeEnd = xRangeEnd;
		this.parA = parA;
		this.parB = parB;
		this.parC = parC;
		this.equation = equation;
		calculateMetricsX();
		findYRange();
		calculateMetricsY();
		metric = calculateMetrics();
		listOfPoints = createListOfPoints();

	}

	private void findYRange() {

		Double y = 0.0;
		for (Double x = xRangeStart; x <= xRangeEnd;) {

			y = giveMeY(x);
			// System.out.println("57 DrawAllEquation.findYRange() x = " + x + "
			// ,y = " +
			// y);
			if (y != null) {
				if (y < yRangeBottom) {
					yRangeBottom = y;
				}
				if (y > yRangeTop) {
					yRangeTop = y;
				}
			} else {
				isPeriodic = true;
			}
			x = (new BigDecimal("" + x).add(new BigDecimal("" + metricX))).doubleValue();
		}
		// System.out.println(
		// "67 DrawAllEquation.findYRange() yRangeBottom = " + yRangeBottom + "
		// ,yRangeTop = " + yRangeTop);
	}

	private ArrayList<Point> createListOfPoints() {
		ArrayList<Point> list = new ArrayList<>();
		Double y = 0.0;
		System.out.println("DrawAllEquation.createListOfPoints()metric " + metric);

		for (Double x = xRangeStart; x <= xRangeEnd;) {

			y = giveMeY(x);
			// System.out.println("80 DrawAllEquation.createListOfPoints() x = "
			// + x + " ,y
			// = " + y);
			if (y != null) {
				list.add(new Point((int) (x / metric), (int) (y / metric)));
			} else {
				list.add(new Point(Integer.MAX_VALUE, Integer.MAX_VALUE));
			}
			x = (new BigDecimal("" + x).add(new BigDecimal("" + metric))).doubleValue();
		}
		if (giveMeY(xRangeEnd) != null) {
			list.add(new Point((int) (xRangeEnd / metric), (int) (giveMeY(xRangeEnd) / metric)));
		} else {
			list.add(new Point(Integer.MAX_VALUE, Integer.MAX_VALUE));
		}

		return list;
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(660, 660);
	}

	public void paintComponent(Graphics g2) {
		Graphics2D g = (Graphics2D) g2;
		super.paintComponent(g);
		this.setBackground(Color.WHITE);
		g.setColor(Color.BLACK);

		// if(isPeriodic) {
		// yRangeBottom = xRangeStart;
		// yRangeTop = xRangeStart;
		// }
		// przeskalowujemy os X i os Y na srodek plana
		makeTransfortmToCartesian(g);

		// axises
		int offset = 30;
		int axisXposAtY = 0;
		int axisXStart = (int) (xRangeStart / metric) - offset;
		int axisXEnd = (int) (xRangeEnd / metric) + offset;

		int axisYposAtX = 0, axisYTop = 0, axisYBottom = 0;
		if (isPeriodic) {
			axisYTop = (int) (yRangeTop / metricY + offset);
			axisYBottom = (int) (yRangeBottom / metricY - offset);
		} else {
			axisYTop = (int) (yRangeTop / metric + offset);
			axisYBottom = (int) (yRangeBottom / metric - offset);
		}
		// move Y axis if range on the left from 0
		if (axisYTop < 0) { // draw x Axis on the Top
			axisXposAtY = axisYTop + 10;
		} else if (axisYBottom > 0) {
			axisXposAtY = axisYBottom - 25;
		}

		// move Y axis if range on the left from 0
		if (axisXStart > 0) {
			axisYposAtX = axisXStart - 40;
		}
		if (axisXEnd < 0) {
			axisYposAtX = axisXEnd + 30;
		}

		// szara kratka
		g.setColor(Color.LIGHT_GRAY);
		for (int y = axisYBottom; y < axisYTop; y++) {
			if (y % 25 == 0) {
				line(g, axisXStart, y, axisXEnd, y);
			}
		}
		for (int x = axisXStart; x < axisXEnd; x++) {
			if (x % 25 == 0) {
				line(g, x, axisYTop, x, axisYBottom);
			}
		}
		g.setColor(Color.BLACK);

		// axisX line
		line(g, axisXStart, axisXposAtY, axisXEnd, axisXposAtY);
		// axisX arrows
		line(g, axisXEnd - 9, axisXposAtY + 4, axisXEnd, axisXposAtY);
		line(g, axisXEnd - 9, axisXposAtY - 4, axisXEnd, axisXposAtY);
		// axisX scale and numbers
		for (int x = axisXStart; x < axisXEnd; x++) {
			if (x % 25 == 0) {
				line(g, x, axisXposAtY + 4, x, axisXposAtY - 4);
				g.drawString("" + dc.format(x * metric), axisXposAtY + 10, x + 4);
			}
		}
		// points on range Start and End on X Axis
		g.setColor(Color.GREEN);
		line(g, (int) (xRangeStart / metric), axisXposAtY + 5, (int) (xRangeStart / metric), axisXposAtY - 5);
		line(g, (int) (xRangeEnd / metric), axisXposAtY + 5, (int) (xRangeEnd / metric), axisXposAtY - 5);
		g.drawString("" + xRangeStart, axisXposAtY + 20, (int) (xRangeStart / metric) + 4);
		g.drawString("" + xRangeEnd, axisXposAtY + 20, (int) (xRangeEnd / metric) + 4);
		g.setColor(Color.BLACK);

		// axisY line
		line(g, axisYposAtX, axisYTop, axisYposAtX, axisYBottom);
		// axisYarrows
		line(g, axisYposAtX - 4, axisYTop - 9, axisYposAtX, axisYTop);
		line(g, axisYposAtX + 4, axisYTop - 9, axisYposAtX, axisYTop);
		// axis Y scale and numbers
		for (int y = axisYBottom; y < axisYTop; y++) {
			if (y % 25 == 0) {
				line(g, axisYposAtX + 4, y, axisYposAtX - 4, y);

				g.setColor(Color.BLACK);
				AffineTransform currTransform = g.getTransform();
				g.rotate(Math.PI / 2);
				g.drawString("" + dc.format(y * metric), axisYposAtX + 10, -y + 4);
				g.setTransform(currTransform);
			}
		}

		/// wykres funkcji
		for (int i = 0; i < listOfPoints.size() - 1; i++) {
			// System.out.println("190 DrawAllEquation.paintComponent()
			// listOfPoints.get(i).x = " + listOfPoints.get(i).x
			// + ", listOfPoints.get(i).y =" + listOfPoints.get(i).y);
			if (!(listOfPoints.get(i).x == Integer.MAX_VALUE || listOfPoints.get(i).y == Integer.MAX_VALUE
					|| listOfPoints.get(i + 1).x == Integer.MAX_VALUE
					|| listOfPoints.get(i + 1).y == Integer.MAX_VALUE)) {
				if (!(listOfPoints.get(i).y > axisYTop || listOfPoints.get(i).y < axisYBottom
						|| listOfPoints.get(i + 1).y > axisYTop || listOfPoints.get(i + 1).y < axisYBottom)) {
					line(g, listOfPoints.get(i).x, listOfPoints.get(i).y, listOfPoints.get(i + 1).x,
							listOfPoints.get(i + 1).y);
				}
			}
		}
	}

	private void calculateMetricsX() {
		if (xRangeStart < 0 && xRangeEnd > 0) {
			metricX = (double) (Math.abs(xRangeStart) + Math.abs(xRangeEnd)) / 500;
		} else if (xRangeStart < 0 && xRangeEnd <= 0) {
			metricX = (double) (Math.abs(xRangeStart) - Math.abs(xRangeEnd)) / 500;
		} else if (xRangeStart >= 0 && xRangeEnd > 0) {
			metricX = (double) (Math.abs(xRangeEnd) - Math.abs(xRangeStart)) / 500;
		}
	}

	private void calculateMetricsY() {
		if (yRangeTop > 0 && yRangeBottom < 0) {
			metricY = (double) (Math.abs(yRangeTop) + Math.abs(yRangeBottom)) / 500;
		} else if (yRangeTop > 0 && yRangeBottom >= 0) {
			metricY = (double) (Math.abs(yRangeTop) - Math.abs(yRangeBottom)) / 500;
		} else if (yRangeTop <= 0 && yRangeBottom < 0) {
			metricY = (double) (Math.abs(yRangeBottom) - Math.abs(yRangeTop)) / 500;
		}
	}

	private double calculateMetrics() {
		if (isPeriodic) {
			return metricX;
		}

		if (metricX >= metricY) {
			return metricX;
		} else {
			return metricY;
		}
	}

	private void makeTransfortmToCartesian(Graphics2D g) {
		int xAxisPosZero = findXaxisPosZero();
		int yAxisPosZero = findYaxisPosZero();
		AffineTransform tform = AffineTransform.getTranslateInstance(xAxisPosZero, yAxisPosZero);
		tform.rotate(Math.PI * 3 / 2, 0, 0);
		g.setTransform(tform);

	}

	private int findYaxisPosZero() {
		int pos = 80;
		if (isPeriodic) {
			pos = (int) ((yRangeTop / metricY) + 80);
		} else {
			pos = (int) ((yRangeTop / metric) + 80);
		}
		return pos;
	}

	private int findXaxisPosZero() {
		int pos = 0;

		if (xRangeStart < 0) {
			pos = (int) (-1 * (xRangeStart / metric) + 80);
		} else if (xRangeStart > 0) {
			pos = (int) (-1 * (xRangeStart / metric) + 80);
		}
		return pos;
	}

	// after rotation of plane order of parameters to
	// y1, x1 , y2, x2,
	private void line(Graphics2D g, int x1, int y1, int x2, int y2) {
		g.drawLine(y1, x1, y2, x2);
	}

	private Double giveMeY(Double x) {
		isPeriodic = true;
		switch (equation) {
		case "cubic":
			isPeriodic = false;
			return calculateQuadraticEquasion(x);
		case "sin":
			return calculateSinX(x);
		case "cos":
			return calculateCosX(x);
		case "tan":
			return calculateTangensX(x);
		case "ctan":
			return calculateCTangensX(x);
		case "ln":
			isPeriodic = false;
			return calculateLogX(x);
		case "oneover":
			return calculateOneOverX(x);
		default:
			isPeriodic = false;
//			System.out.println("x------ " + x);
			String repEquation = equation.replaceAll("x", "" + x);
//			System.out.println();
//			System.out.println("304 equation ------ " + repEquation);
			try {
				return Double.parseDouble(ShuntingYardAlgorithm.allFunctions(repEquation, false));
			} catch (Exception e) {
				return null;
			}

		}

		// return calculateQuadraticEquasion(x);
		// return calculateOneOverX(x);
		// return y;
		// return calculateTangensX(x);
		// return calculateSinX(x);
	}

	private Double calculateLogX(double x) {
		if (x <= 0) {
			return null;
		} else {
			return parA * Math.log(parB * x);
		}
	}

	private Double calculateTangensX(double x) {
		return parA * Math.tan(parB * x);
	}

	private Double calculateCTangensX(double x) {
		return 1.0 / Math.tan(x);
	}

	private Double calculateOneOverX(double x) {
		if (x == 0) {
			return null;
		}
		return (parA / (parB * x));
	}

	private Double calculateSinX(double x) {

		return parA * Math.sin(parB * x);
	}

	private Double calculateCosX(double x) {

		return parA * Math.cos(parB * x);
	}

	private Double calculateQuadraticEquasion(double x) {
		return (parA * x * x) + (parB * x) + parC;
	}

}