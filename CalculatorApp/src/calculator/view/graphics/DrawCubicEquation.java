package calculator.view.graphics;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.AffineTransform;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.swing.JPanel;

public class DrawCubicEquation extends JPanel {
	DecimalFormat dc = new DecimalFormat("#.##");
	private double xRangeEnd, xRangeStart, yRangeTop, yRangeBottom, parA, parB, parC, parD, metric;
	ArrayList<Point> listOfPoints;

	public DrawCubicEquation(double xRangeStart, double xRangeEnd, double parA, double parB, double parC, double parD) {
		setPreferredSize(getPreferredSize());
		this.xRangeStart = xRangeStart;
		this.xRangeEnd = xRangeEnd;
		this.parA = parA;
		this.parB = parB;
		this.parC = parC;
		this.parD = parD;

		yRangeTop = findYRangeTop();
		yRangeBottom = findYRangeBottom();
		metric = calculateMetrics(xRangeStart, xRangeEnd, yRangeTop, yRangeBottom);
//		System.out.println("DrawQuadraticEquation.DrawQuadraticEquation() xRangeStart " + xRangeStart);
//		System.out.println("DrawQuadraticEquation.DrawQuadraticEquation() xRangeEnd " + xRangeEnd);
//		System.out.println("DrawQuadraticEquation.DrawQuadraticEquation() yRangeTop " + yRangeTop);
//		System.out.println("DrawQuadraticEquation.DrawQuadraticEquation() yRangeBottom " + yRangeBottom);
//		System.out.println("DrawQuadraticEquation.DrawQuadraticEquation() metric " + metric);
		listOfPoints = createListOfPoints();
		repaint();
		revalidate();
	}

	private ArrayList<Point> createListOfPoints() {
		ArrayList<Point> list = new ArrayList<>();
		for (double x = xRangeStart; x <= xRangeEnd + metric; x += metric) {
			double y = calculateCubicEquasion(x);
			list.add(new Point((int) (x / metric), (int) (y / metric)));
		}
		return list;
	}

	private double findYRangeTop() {
		double vertix1 = 0;
		double vertix2 = 0;

		if (parA != 0) {
			System.out.println( Math.pow(parA, 2));
			vertix1 = (-parB - Math.sqrt(parA * parA + 4 * parA * parB))/2 * parA ;
			vertix2 = (-parB + Math.sqrt(Math.pow(parA, 2) + 4 * parA * parB)) / 2 * parA;
			System.out.println("vertix1= "+vertix1);
			System.out.println("vertix2= "+vertix2);
		}
		// System.out.println("vertix " + vertix);
		double YonRangeStart = calculateCubicEquasion(xRangeStart);
		double YonRangeEnd = calculateCubicEquasion(xRangeEnd);
		double YonVertix = calculateCubicEquasion(vertix1);

		double max = YonRangeStart;
		if (max < YonRangeEnd) {
			max = YonRangeEnd;
		}
		if (max < YonVertix && vertix1 > xRangeStart && vertix1 < xRangeEnd) {
			max = YonVertix;
		}
		return max;
	}

	private double findYRangeBottom() {
		double vertix1 = 0;
		double vertix2 = 0;

		if (parA != 0) {
//			vertix1 = (-parB + Math.sqrt( Math.pow(parA, 2) - (4 * parA * parB))) / 2 * parA;
//			vertix2 = (-parB - Math.sqrt(Math.pow(parA, 2) - 4 * parA * parB)) / 2 * parA;
//			System.out.println(vertix1 + " " + vertix2);
		}

		System.out.println("vertix " + vertix1);
		double YonRangeStart = calculateCubicEquasion(xRangeStart);
		double YonRangeEnd = calculateCubicEquasion(xRangeEnd);
		double YonVertix = calculateCubicEquasion(vertix1);

		double min = YonRangeStart;
		if (min > YonRangeEnd) {
			min = YonRangeEnd;
		}
		if (min > YonVertix && vertix1 > xRangeStart && vertix1 < xRangeEnd) {
			min = YonVertix;
		}
		return min;
	}
	private double calculateCubicEquasion(double x) {
		return parA * Math.pow(x, 3) + parB * Math.pow(x, 2) + parC * x + parD;

	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(660, 660);
	}

	public void paintComponent(Graphics g2) {
		Graphics2D g = (Graphics2D) g2;
		super.paintComponent(g);
		this.setBackground(Color.WHITE);
		g.setColor(Color.BLACK);

		// przeskalowujemy os X i os Y na srodek plana
		makeTransfortmToCartesian(g);

		// axises
		int offset = 30;
		int axisXposAtY = 0;
		int axisXStart = (int) (xRangeStart / metric) - offset;
		int axisXEnd = (int) (xRangeEnd / metric) + offset;

		int axisYposAtX = 0;
		int axisYTop = (int) (yRangeTop / metric + offset);
		int axisYBottom = (int) (yRangeBottom / metric - offset);

		// move Y axis if range on the left from 0
		if (axisYTop < 0) { // draw x Axis on the Top
			axisXposAtY = axisYTop + 10;
		} else if (axisYBottom > 0) {
			axisXposAtY = axisYBottom - 25;
		}

		// move Y axis if range on the left from 0
		if (axisXStart > 0) {
			axisYposAtX = axisXStart - 40;
		}
		if (axisXEnd < 0) {
			axisYposAtX = axisXEnd + 30;
		}

		// szara kratka
		g.setColor(Color.LIGHT_GRAY);
		for (int y = axisYBottom; y < axisYTop; y++) {
			if (y % 25 == 0) {
				line(g, axisXStart, y, axisXEnd, y);
			}
		}
		for (int x = axisXStart; x < axisXEnd; x++) {
			if (x % 25 == 0) {
				line(g, x, axisYTop, x, axisYBottom);
			}
		}
		g.setColor(Color.BLACK);

		// axisX line
		line(g, axisXStart, axisXposAtY, axisXEnd, axisXposAtY);
		// axisX arrows
		line(g, axisXEnd - 9, axisXposAtY + 4, axisXEnd, axisXposAtY);
		line(g, axisXEnd - 9, axisXposAtY - 4, axisXEnd, axisXposAtY);
		// axisX scale and numbers
		for (int x = axisXStart; x < axisXEnd; x++) {
			if (x % 25 == 0) {
				line(g, x, axisXposAtY + 4, x, axisXposAtY - 4);
				g.drawString("" + dc.format(x * metric), axisXposAtY + 10, x + 4);
			}
		}
		// points on range Start and End on X Axis
		g.setColor(Color.GREEN);
		line(g, (int) (xRangeStart / metric), axisXposAtY + 5, (int) (xRangeStart / metric), axisXposAtY - 5);
		line(g, (int) (xRangeEnd / metric), axisXposAtY + 5, (int) (xRangeEnd / metric), axisXposAtY - 5);
		g.drawString("" + xRangeStart, axisXposAtY + 20, (int) (xRangeStart / metric) + 4);
		g.drawString("" + xRangeEnd, axisXposAtY + 20, (int) (xRangeEnd / metric) + 4);
		g.setColor(Color.BLACK);

		// axisY line
		line(g, axisYposAtX, axisYTop, axisYposAtX, axisYBottom);
		// axisYarrows
		line(g, axisYposAtX - 4, axisYTop - 9, axisYposAtX, axisYTop);
		line(g, axisYposAtX + 4, axisYTop - 9, axisYposAtX, axisYTop);
		// axis Y scale and numbers
		for (int y = axisYBottom; y < axisYTop; y++) {
			if (y % 25 == 0) {
				line(g, axisYposAtX + 4, y, axisYposAtX - 4, y);

				g.setColor(Color.BLACK);
				AffineTransform currTransform = g.getTransform();
				g.rotate(Math.PI / 2);
				g.drawString("" + dc.format(y * metric), axisYposAtX + 10, -y + 4);
				g.setTransform(currTransform);
			}
		}

		/// wykres funkcji
		for (int i = 0; i < listOfPoints.size() - 1; i++) {
			line(g, listOfPoints.get(i).x, listOfPoints.get(i).y, listOfPoints.get(i + 1).x, listOfPoints.get(i + 1).y);
		}
	}

	private double calculateMetrics(double xRangeStart, double xRangeEnd, double yRangeTop, double yRangeBootom) {
		double metricX = 0.0;
		double metricY = 0.0;
		if (xRangeStart < 0 && xRangeEnd > 0) {
			metricX = (double) (Math.abs(xRangeStart) + Math.abs(xRangeEnd)) / 500;
		} else if (xRangeStart < 0 && xRangeEnd <= 0) {
			metricX = (double) (Math.abs(xRangeStart) - Math.abs(xRangeEnd)) / 500;
		} else if (xRangeStart >= 0 && xRangeEnd > 0) {
			metricX = (double) (Math.abs(xRangeEnd) - Math.abs(xRangeStart)) / 500;
		}

		if (yRangeTop > 0 && yRangeBootom < 0) {
			metricY = (double) (Math.abs(yRangeTop) + Math.abs(yRangeBootom)) / 500;
		} else if (yRangeTop > 0 && yRangeBootom >= 0) {
			metricY = (double) (Math.abs(yRangeTop) - Math.abs(yRangeBootom)) / 500;
		} else if (yRangeTop <= 0 && yRangeBootom < 0) {
			metricY = (double) (Math.abs(yRangeBootom) - Math.abs(yRangeTop)) / 500;
		}
		// System.out.println("TestGraphics.calculateMetrics()metricX " +
		// metricX);
		// System.out.println("TestGraphics.calculateMetrics()metricY " +
		// metricY);
		if (metricX >= metricY) {
			return metricX;
		} else {
			return metricY;
		}
	}

	private void makeTransfortmToCartesian(Graphics2D g) {
		int xAxisPosZero = findXaxisPosZero();
		int yAxisPosZero = findYaxisPosZero();
		AffineTransform tform = AffineTransform.getTranslateInstance(xAxisPosZero, yAxisPosZero);
		tform.rotate(Math.PI * 3 / 2, 0, 0);
		g.setTransform(tform);

	}

	private int findYaxisPosZero() {
		int pos = 80;

		pos = (int) ((yRangeTop / metric) + 80);
		return pos;
	}

	private int findXaxisPosZero() {
		int pos = 0;

		if (xRangeStart < 0) {
			pos = (int) (-1 * (xRangeStart / metric) + 80);
		} else if (xRangeStart > 0) {
			pos = (int) (-1 * (xRangeStart / metric) + 80);
		}
		return pos;
	}

	// after rotation of plane order of parameters to
	// y1, x1 , y2, x2,
	private void line(Graphics2D g, int x1, int y1, int x2, int y2) {
		g.drawLine(y1, x1, y2, x2);
	}
}