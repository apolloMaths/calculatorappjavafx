package calculator.view;

import java.io.IOException;
import java.net.URL;

import calculator.MainApp;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

/**
 * Note that we load the panes with the FXMLLoader on every use. This allows us
 * to manipulate the CSS between loads and have it take affect.
 *
 * Also, the panes should not save state internally. Reloading the FXML forces
 * better programming design, because it is impossible to get lazy and expect
 * the panes to save their own state.
 */
public class MenusControl {

	@FXML // fx:id="displayOne"
	private MenuItem equationMenu; // Value injected by FXMLLoader

	@FXML // fx:id="displayTwo"
	private MenuItem graphingMenu; // Value injected by FXMLLoader

	/**
	 * Event handler for MenuItem one
	 */
	@FXML
	void switchToEquationView(ActionEvent event) {
		try {
			System.out.println("Menu called: EquationView");
			URL paneOneUrl = getClass().getResource("EquationView.fxml");
			AnchorPane paneOne = FXMLLoader.load(paneOneUrl);

			BorderPane border = MainApp.getRoot();

			border.setCenter(paneOne);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Event handler for MenuItem Cubic Eq Graph
	 */
	@FXML
	void switchToCubicEqView(ActionEvent event) {

		try {
			URL paneTwoUrl = getClass().getResource("CubicEqGraph.fxml");
			AnchorPane paneTwo = FXMLLoader.load(paneTwoUrl);

			BorderPane border = MainApp.getRoot();
			border.setCenter(paneTwo);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	/**
	 * Event handler for MenuItem All Eq Graph
	 */
	@FXML
	void switchToAllEqView(ActionEvent event) {

		try {
			URL paneTwoUrl = getClass().getResource("AllEqGraph.fxml");
			AnchorPane paneTwo = FXMLLoader.load(paneTwoUrl);

			BorderPane border = MainApp.getRoot();
			border.setCenter(paneTwo);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Event handler for MenuItem Quadratic Eq Graph
	 */
	@FXML
	void switchToQuadraticEqView(ActionEvent event) {

		try {
			URL paneTwoUrl = getClass().getResource("QuadraticEqGraph.fxml");
			AnchorPane paneTwo = FXMLLoader.load(paneTwoUrl);

			BorderPane border = MainApp.getRoot();
			border.setCenter(paneTwo);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Event handler for MenuItem Linear Eq Graph
	 */
	@FXML
	void switchToLinearEqView(ActionEvent event) {

		try {
			URL paneTwoUrl = getClass().getResource("LinearEqGraph.fxml");
			AnchorPane paneTwo = FXMLLoader.load(paneTwoUrl);

			BorderPane border = MainApp.getRoot();
			border.setCenter(paneTwo);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Event handler for MenuItem Haar
	 */
	@FXML
	void switchToHaarView(ActionEvent event) {

		try {
			URL paneTwoUrl = getClass().getResource("Haar.fxml");
			AnchorPane paneTwo = FXMLLoader.load(paneTwoUrl);

			BorderPane border = MainApp.getRoot();
			border.setCenter(paneTwo);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	@FXML
	void switchToHaarImageView(ActionEvent event) {

		try {
			URL paneTwoUrl = getClass().getResource("HaarImage.fxml");
			AnchorPane paneTwo = FXMLLoader.load(paneTwoUrl);

			BorderPane border = MainApp.getRoot();
			border.setCenter(paneTwo);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Event handler for MenuItem CompressionDecompression
	 */
	@FXML
	void switchToCompressionDecompression(ActionEvent event) {

		try {
			URL paneTwoUrl = getClass().getResource("CompressionDecompression.fxml");
			AnchorPane paneTwo = FXMLLoader.load(paneTwoUrl);

			BorderPane border = MainApp.getRoot();
			border.setCenter(paneTwo);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Event handler for MenuItem Matrices
	 */
	@FXML
	void switchToMatricesView(ActionEvent event) {

		try {
			URL paneTwoUrl = getClass().getResource("Matrices.fxml");
			AnchorPane paneTwo = FXMLLoader.load(paneTwoUrl);

			BorderPane border = MainApp.getRoot();
			border.setCenter(paneTwo);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@FXML
	void switchToPermutationsView(ActionEvent event) {

		try {
			URL paneTwoUrl = getClass().getResource("PermutationsView.fxml");
			AnchorPane paneTwo = FXMLLoader.load(paneTwoUrl);

			BorderPane border = MainApp.getRoot();
			border.setCenter(paneTwo);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}