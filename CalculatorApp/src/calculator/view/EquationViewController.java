package calculator.view;
import calculator.MainApp;
import calculator.model.Equation;
import calculator.util.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.Alert.AlertType;

public class EquationViewController {

	// Reference to the main application.
    private ObservableList<Equation> equationData = FXCollections.observableArrayList();

	ShuntingYardAlgorithm algo = new ShuntingYardAlgorithm();
	@FXML
    private TextField equationField;

	@FXML
	private TableView<Equation> equationTable;

	@FXML
	private TableColumn<Equation,String> equationColumn;

	@FXML
	private ToggleButton radianOrDegreesButton;

	private boolean radiansOrDegrees = false;

	@FXML
    private void handle1() {
		equationField.setText(equationField.getText() + "1");
    }

	@FXML
    private void handle2() {
		equationField.setText(equationField.getText() + "2");
    }

	@FXML
    private void handle3() {
		equationField.setText(equationField.getText() + "3");
    }


	@FXML
    private void handle4() {
		equationField.setText(equationField.getText() + "4");
    }


	@FXML
    private void handle5() {
		equationField.setText(equationField.getText() + "5");
    }


	@FXML
    private void handle6() {
		equationField.setText(equationField.getText() + "6");
    }


	@FXML
    private void handle7() {
		equationField.setText(equationField.getText() + "7");
    }

	@FXML
    private void handle8() {
		equationField.setText(equationField.getText() + "8");
    }


	@FXML
    private void handle9() {
		equationField.setText(equationField.getText() + "9");
    }

	@FXML
    private void handle0() {
		equationField.setText(equationField.getText() + "0");
    }

	@FXML
    private void handleDecPoint() {
		equationField.setText(equationField.getText() + ".");
    }

	@FXML
    private void handlePlus() {
		equationField.setText(equationField.getText() + " + ");
    }

	@FXML
    private void handleMinus() {
		equationField.setText(equationField.getText() + " - ");
    }

	@FXML
    private void handleMultiply() {
		equationField.setText(equationField.getText() + " * ");
    }

	@FXML
    private void handleDivide() {
		equationField.setText(equationField.getText() + " / ");
    }

	@FXML
    private void handleSin() {
		equationField.setText(equationField.getText() + "sin ( ");
    }

	@FXML
    private void handleCos() {
		equationField.setText(equationField.getText() + "cos ( ");
    }

	@FXML
    private void handleTan() {
		equationField.setText(equationField.getText() + "tan ( ");
    }

	@FXML
    private void handleLeftPar() {
		equationField.setText(equationField.getText() + "( ");
    }

	@FXML
    private void handleRightPar() {
		equationField.setText(equationField.getText() + " )");
    }

	@FXML
    private void handleExp() {
		equationField.setText(equationField.getText() + "exp ( ");
    }

	@FXML
    private void handleLn() {
		equationField.setText(equationField.getText() + "ln ( ");
    }

	@FXML
    private void handleLog() {
		equationField.setText(equationField.getText() + "log ( ");
    }

	@FXML
    private void handleClear() {
		equationField.setText("");
    }

	@FXML
    private void handleDel() {
		equationField.setText(equationField.getText().substring(0, equationField.getText().length()-1));
    }

	@FXML
    private void handlePI() {
		equationField.setText(equationField.getText()+" 3.14159265359");
    }

	@FXML
    private void handleEquals() {
		boolean passed = false;
		String equation = equationField.getText();
		int opening =0,closing = 0;
		for(char c : equation.toCharArray()){
			if(c == '('){
				opening++;
			}
			else if(c == ')'){
				closing++;
			}
		}
		if(opening == closing){
			passed = true;
		}
		if(passed){
			Equation newEq =  new Equation("test",equationField.getText());
			equationData.add(0, newEq);
			String result = algo.allFunctions(equationField.getText(),radiansOrDegrees);
			double result1 = Double.parseDouble(result);
			if(result1 % 1 == 0){
				equationField.setText(""+(int)result1);
			}
			else{
				equationField.setText(result);
			}
		}
		else {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText("Missing brackets");
			alert.setContentText("Please check for missing brackets");
			alert.showAndWait();
		}
    }

	@FXML
	private void handlePower(){
		equationField.setText(equationField.getText() + " ^ ");
	}

	@FXML
	private void handleSqrt(){
		equationField.setText(equationField.getText() + "sqrt (");
	}

	@FXML
	private void handleUnary(){
		equationField.setText(equationField.getText() + "-");
	}

	@FXML
    private void handleRadiansOrDegrees() {
		if(radiansOrDegrees){
			radiansOrDegrees = false;
			radianOrDegreesButton.setText("Radians");
		}
		else{
			radiansOrDegrees = true;
			radianOrDegreesButton.setText("Degrees");
		}
		System.out.println("Toggle: " + radiansOrDegrees);
    }

	@FXML
    private void initialize() {
        // Initialize the person table with the two columns.
        equationColumn.setCellValueFactory(cellData -> cellData.getValue().equationProperty());

     // Listen for selection changes and show the person details when changed.
        equationTable.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> showEquationDetails(newValue));
        equationData.add(new Equation("Test", "1 + 2"));
        equationData.add(new Equation("Test", "1 + 2 + 5"));
        equationTable.setItems(equationData);
    }

	private void showEquationDetails(Equation equationParam) {
        if (equationParam != null) {
            // Fill the labels with info from the person object.
            equationField.setText(equationParam.getEquation());
        } else {
            // Person is null, remove all the text.
            equationField.setText("");
        }
    }


}
