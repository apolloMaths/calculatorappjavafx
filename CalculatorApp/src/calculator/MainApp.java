package calculator;

import java.io.IOException;

import calculator.model.Equation;
import calculator.view.CubicEqGraphController;
import calculator.view.EquationViewController;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class MainApp extends Application {
// MainApp finished
    private Stage primaryStage;
    private static BorderPane rootLayout;



    private ObservableList<Equation> equationData = FXCollections.observableArrayList();

    public MainApp() {
        // Add some sample data
        equationData.add(new Equation("Test", "1 + 2"));
        equationData.add(new Equation("Test", "1 + 2 + 5"));
    }

    public static BorderPane getRoot() {
        return rootLayout;
      }

    public ObservableList<Equation> getEquationData() {
        return equationData;
    }
    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Calculator");

        initRootLayout();

        showEquationOverview();
    }
    public void showEquationOverview() {
        try {
            // Load person overview.
        	FXMLLoader loader = new FXMLLoader();
        	loader.setLocation(MainApp.class.getResource("view/EquationView.fxml"));
        	//loader.setLocation(MainApp.class.getResource("view/CubicEqGraph.fxml"));
            AnchorPane personOverview = (AnchorPane) loader.load();

            // Set person overview into the center of root layout.
            rootLayout.setCenter(personOverview);
            //CubicEqGraphController controller = loader.getController();
            EquationViewController controller = loader.getController();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * Initializes the root layout.
     */
    public void initRootLayout() {
        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/RootLayout.fxml"));
            rootLayout = (BorderPane) loader.load();

            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.setResizable(false);
            primaryStage.show();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns the main stage.
     * @return
     */
    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public static void main(String[] args) {
        launch(args);
    }
}