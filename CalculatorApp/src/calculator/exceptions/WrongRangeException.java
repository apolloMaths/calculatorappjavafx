package calculator.exceptions;

public class WrongRangeException extends Exception {
	public WrongRangeException(String msg){
		super(msg);
	}
}
